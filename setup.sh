#!/bin/sh
# ################################################
rpm_rte="rpm.rte"
rpm_wget="wget-1.9.1-1.aix5.1.ppc.rpm"
# ################################################
# Setup rpm and wget
# ################################################
function setup_rpm {
  rm /usr/bin/rpm
  if ! test -e "/QOpenSys/download/$rpm_rte"; then
    mkdir /QOpenSys/download
    echo "setup failure: move $rpm_rte and $rpm_wget to /QOpenSys/download"
    return 3
  fi
  cd /QOpenSys/download
  if ! test -e "$rpm_rte"; then
    echo "setup failure: missing $rpm_rte setup failure"
    return 2
  fi
  if test -e /usr/bin/rpm; then
    echo "setup failure: /usr/bin/rpm already installed"
    return 1
  fi
  echo "setting up $rpm_rte ..."
  restore -xvqf $rpm_rte
  mkdir /QOpenSys/opt
  cp -R /QOpenSys/download/usr/opt/* /QOpenSys/opt/.
  cd /QOpenSys/download
  rm -R usr
  ln -s /QOpenSys/opt /QOpenSys/var/opt
  ln -s /QOpenSys/opt /opt
  ln -s /QOpenSys/var/opt /var/opt
  ln -s /opt/freeware/bin/rpm /usr/bin/rpm
  cd /opt/freeware/lib
  ln -s libpopt.so.0.0.0 libpopt.so
  ln -s librpm.so.0.0.0 librpm.so
  ln -s librpmbuild.so.0.0.0 librpmbuild.so
}
function setup_wget {
  cd /QOpenSys/download
  if ! test -e "/QOpenSys/download/$rpm_wget"; then
    echo "setup failure:  move $rpm_wget to /QOpenSys/download"
    return 4
  fi
  echo "setting up $rpm_wget ..."
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv $rpm_wget
}

function setup_prezl {
  echo "setting up prezl depend tree ..."
  tar -xf www.oss4aix.org.deps.tar
}
# ################################################
# Main
# ################################################
setup_rpm
setup_wget
setup_prezl


