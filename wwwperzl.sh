#!/bin/sh
# PERZL_DEBUG="1"
PERZL_RELEASE_AIX53="aix53"
PERZL_RELEASE_AIX61="aix61"
PERZL_LOCAL_BLACKLIST="::"

if [ -d /QOpenSys/usr/bin ]
then
  system_OS400=1
  # setup paths to IBM Open source binaries and libraries 
  # Notes: https://bitbucket.org/litmis/ibmichroot/issues/8/alternative-download-pkg_setupsh-on-linux
  PATH=/QOpenSys/usr/bin:/QOpenSys/usr/sbin:/opt/freeware/bin
  LIBPATH=/QOpenSys/usr/lib:/opt/freeware/lib
  export PATH
  export LIBPATH
  echo "**********************"
  echo "Live IBM i session (changes made)."
  echo "**********************"
  echo "PATH=$PATH"
  echo "LIBPATH=$LIBPATH"
else
  system_OS400=0
  echo "**********************"
  echo "Not IBM i, downloads only."
  echo "**********************"
fi


# assign_global_vars $1
#   $1 - aix53|aix61
# output
#   na
function assign_global_vars {
  PERZL_RELEASE_INUSE=$1
  PERZL_WGET_LIST_DEPS="ftp://www.oss4aix.org/rpmdb/deplists/$PERZL_RELEASE_INUSE/"
  PERZL_WGET_LIST_RPMS="http://www.oss4aix.org/download/RPMS/"
  PERZL_LOCAL_COPY_RPMS="www.oss4aix.org.site.dirs"
  PERZL_LOCAL_COPY_DEPS="www.oss4aix.org/rpmdb/deplists/$PERZL_RELEASE_INUSE/"
  PERZL_LOCAL_PREVLIST=$PERZL_LOCAL_BLACKLIST
}

# help_user
#   na
# output
#   help user
function help_user {
  echo " "
  echo "customer:"
  echo "--------------"
  echo "wwwperzl.sh $PERZL_RELEASE_AIX53|$PERZL_RELEASE_AIX61 option [scan]"
  echo "required:"
  echo " $PERZL_RELEASE_AIX53|$PERZL_RELEASE_AIX61"
  echo "option"
  echo " help          - help customer"
  echo " admin         - admin help, PASE releases, perzl site copy functions, etc."
  echo " fix           - fix freeware/lib/libiconv.a (post rpm)"
  echo " list   [scan] - list possible downloads"
  echo " count  [scan] - count list possible downloads"
  echo " wget1  [scan] - wget from perzl"
  echo " wget   [scan] - wget plus depends from perzl"
  echo " wgetv  [scan] - wget plus depends from perzl (view action only)"
  echo " rpm1   [scan] - rpm install local (post wget)"
  echo " rpm    [scan] - rpm install plus depends (post wget)"
  echo " rpmv   [scan] - rpm install plus depends (view action only)"
  echo "                (*)[scan] - wild card rpm scan [gcc*]"
  echo " "
  echo "pase->aix release:"
  echo " --------------"
  echo " V5R3 -> aix52"
  echo " V5R4 -> aix53"
  echo " V6R1 -> aix53"
  echo " V7R1 -> aix61"
}

# help_admin
#   na
# output
#   help admin
function help_admin {
  help_user
  echo " "
  echo "internal use:"
  echo " --------------"
  echo "option"
  echo " bigdeps       - local perzl site map wget www.oss4aix.org/rpmdb/deplists/($PERZL_RELEASE_AIX53,$PERZL_RELEASE_AIX61)/*.deps (files)"
  echo " bigrpms       - local perzl site map wget ftp://www.oss4aix.org/RPMS/../*.rpm (names only)"
  echo "                 (*) local perzl site map included download, do not run bigxxx (very long copy)"
  echo " "
  echo "perzl release:"
  echo " --------------"
  echo " aix release inuse....$PERZL_RELEASE_INUSE ($PERZL_RELEASE_AIX53,$PERZL_RELEASE_AIX61)"
  echo " local perzl deps.....$PERZL_LOCAL_COPY_DEPS ($PERZL_WGET_LIST_DEPS)"
  echo " local perzl rpms.....$PERZL_LOCAL_COPY_RPMS ($PERZL_WGET_LIST_RPMS)"
}


# #####################################
# helper functions
# #####################################

# execute_cmd $1
#   $1 - cmd to execute
# output
#   cmd result
function execute_cmd {
  # echo "$1"
  $1
}

# replace_filetype_deps_with_rpm $1
#   $1 - name of download dep
# output
#   replace .deps with .rpm
# example
#   $1="bash-4.2-12.aix5.1.ppc.deps"
#   output "bash-4.2-12.aix5.1.ppc.rpm"
function replace_filetype_deps_with_rpm {
  echo $1 | awk '{ gsub(/.deps/,".rpm"); print }'
}

# replace_filetype_rpm_with_deps $1
#   $1 - name of download rpm
# output
#   replace .rpm with .deps
# example
#   $1="bash-4.2-12.aix5.1.ppc.rpm"
#   output "bash-4.2-12.aix5.1.ppc.deps"
function replace_filetype_rpm_with_deps {
  echo $1 | awk '{ gsub(/.rpm/,".deps"); print }'
}

# remove_filetype_rpm $1
#   $1 - name of download rpm
# output
#   remove .rpm
# example
#   $1="bash-4.2-12.aix5.1.ppc.rpm"
#   output "bash-4.2-12.aix5.1.ppc"
function remove_filetype_rpm {
  echo $1 | awk '{ gsub(/.rpm/,""); print }'
}

# remove_filetype_deps $1
#   $1 - name of download deps
# output
#   remove .deps
# example
#   $1="bash-4.2-12.aix5.1.ppc.deps"
#   output "bash-4.2-12.aix5.1.ppc"
function remove_filetype_deps {
  echo $1 | awk '{ gsub(/.deps/,""); print }'
}


# find_rpm_local $1
#   $1 - name of download rpm/deps
# output
#   1 - match found in current dir
#   0 - not match in current dir
# example  
#    $1="bash-4.2-12.aix5.1.ppc.rpm"
#    output 0 (not exist this directory)
#    -- or --
#    output 1 (exist this directory)
function find_rpm_local {
  rpm=$(replace_filetype_deps_with_rpm $1)
  if test -e $rpm
  then
    echo "1"
  else
    echo "0"
  fi
}


# #####################################
# perzl action functions 
# require files ./www.oss4aix.org/..
#   oss4aix_perzl_deps_download
#   oss4aix_perzl_subdir_download
# #####################################


# oss4aix_master_rpm_name $1
#   $1 - name of download rpm/dep
# output
#   rpm name from www.oss4aix.org.site.dirs
# example
#    $1="bash-4.2-12.aix5.1.ppc.deps"
#    output "bash-4.2-12.aix5.1.ppc.rpm"
function oss4aix_master_rpm_name {
  rpm=$(replace_filetype_deps_with_rpm $1)
  short=$(remove_filetype_rpm $rpm)
  base=$(grep  "[[:space:]]$rpm" $PERZL_LOCAL_COPY_RPMS | awk '{print $2}')
  echo $base
}

# oss4aix_master_rpm_dir $1
#   $1 - name of download rpm/dep
# output
#   rpm name from www.oss4aix.org.site.dirs
# example
#    $1="bash-4.2-12.aix5.1.ppc.deps"
#    output "bash"
function oss4aix_master_rpm_dir {
  rpm=$(replace_filetype_deps_with_rpm $1)
  short=$(remove_filetype_rpm $rpm)
  base=$(grep "[[:space:]]$rpm" $PERZL_LOCAL_COPY_RPMS | awk '{print $1}')
  echo $base
}


# oss4aix_list_display $1
#   $1 - wild card wild*.deps
# output
#   list matching wild*.deps
# note
#   assumes ./www.oss4aix.org/..
function oss4aix_list_display {
  pop=$(pwd)
  cd $PERZL_LOCAL_COPY_DEPS
  execute_cmd "ls -1 $1*.deps"
  execute_cmd "cd $pop"
}

# oss4aix_list_count $1
#   $1 - wild card wild*.deps
# output
#   nbr match files "wild*.deps"
# note
#   assumes ./www.oss4aix.org/..
function oss4aix_list_count {
  echo $(oss4aix_list_display $1 | tr -s ' ' '\n' | grep -c $1)
}

# oss4aix_wget_site $1
#   $1 - name of download rpm
# output
#   na
# note
#   wget an rpm from perzl site
#   assumes ./www.oss4aix.org/..
# example
#    $1="bash-4.2-12.aix5.1.ppc.deps"
#    -- or --
#    $1="bash-4.2-12.aix5.1.ppc.rpm"
#    wget "http://www.oss4aix.org/../bash/bash-4.2-12.aix5.1.ppc.rpm"
function oss4aix_wget_site {
  fnd=$(find_rpm_local $1)
  if (($fnd == 0))
  then
    root=$(oss4aix_master_rpm_dir $1)
    rpm=$(replace_filetype_deps_with_rpm $1)
    if [ -z "$PERZL_DEBUG" ]
    then
      execute_cmd "wget $PERZL_WGET_LIST_RPMS$root/$rpm"
    else
      echo "wget $PERZL_WGET_LIST_RPMS$root/$rpm"
    fi
  else
    rpm=$(replace_filetype_deps_with_rpm $1)
    echo "rpm [$rpm] previously downloaded"
    execute_cmd "ls -l $rpm"
  fi
}

# oss4aix_fix_libiconv
#   na
# output
#   na
function oss4aix_fix_libiconv {
  if test -e /opt/freeware/lib/libiconv.a
  then
    cnt=$(ar -t /opt/freeware/lib/libiconv.a | grep -c shr4)
    if (($cnt==0))
    then
      echo "fixing /opt/freeware/lib/libiconv.a ..."
      ar -x /usr/lib/libiconv.a
      ar -rv /opt/freeware/lib/libiconv.a shr4.o
      ar -rv /opt/freeware/lib/libiconv.a shr.o
      echo "... complete"
    else
      echo "/opt/freeware/lib/libiconv.a fixed (ok)"
    fi
  fi
}

# oss4aix_rpm_install $1
#   $1 - name of download rpm
# output
#   na
# note
#   rpm install local *.rpm
#   assumes ./www.oss4aix.org/..
# example
#    $1="bash-4.2-12.aix5.1.ppc.deps"
#    -- or --
#    $1="bash-4.2-12.aix5.1.ppc.rpm"
#    rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv bash-4.2-12.aix5.1.ppc.rpm
function oss4aix_rpm_install {
  rpm=$(replace_filetype_deps_with_rpm $1)
  if [ -z "$PERZL_DEBUG" ]
  then
    execute_cmd "rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv $rpm"
    # fix iconv (sigh)
    cnt=$(echo "$rpm" | grep -c libiconv)
    if (($cnt==1))
    then
      oss4aix_fix_libiconv
    fi
  else
    echo "rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv $rpm"
  fi
}

# oss4aix_single $1 $2
#   $1 - operation (1="wget",2="rpm")
#   $2 - wild card wild*.deps
# output
#   na
function oss4aix_single {
  unique=$(oss4aix_list_count $2)
  if (($unique == 1))
  then
    name=$(oss4aix_list_display $2)
    if (($1 == 1))
    then
      oss4aix_wget_site $name
    else
      oss4aix_rpm_install $name
    fi
  else
    echo "search [$2] unique version not found"
    oss4aix_list_display $2
  fi
}

# oss4aix_tree $1 $2
#   $1 - operation (1="wget",2="rpm")
#   $2 - wild card wild*.deps
# output
#   na
function oss4aix_tree {
  unique=$(oss4aix_list_count $2)
  if (($unique == 1))
  then
    name=$(oss4aix_list_display $2)
    # echo "parent depends on $name ..."
    while read depend
    do
      # echo "child operation $depend ..."
      next=$(remove_filetype_rpm $depend)
      inlist=$(echo "$PERZL_LOCAL_PREVLIST" | grep -c ":$next:")
      if (($inlist == 0))
      then
        PERZL_LOCAL_PREVLIST="$PERZL_LOCAL_PREVLIST:$next:"
        oss4aix_single $1 $next
        oss4aix_tree $1 $next
      fi
    done < $PERZL_LOCAL_COPY_DEPS""$name
  else
    echo "search [$2] unique version not found"
    oss4aix_list_display $2
  fi
}

# INTERNAL USE -- prepare download
# oss4aix_perzl_deps_download
# output
#   create ./www.oss4aix.org/..
# One time operation:
#   PERZL_WGET_LIST_DEPS db included download,
#   so customer need never run
#   oss4aix_perzl_deps_download.
#   Routine web drags depend PERZL_WGET_LIST_DEPS 
#   from perzl site to create 
#   directory www.oss4aix.org/.. 
#   (very, very loooooooong running task)
function oss4aix_perzl_deps_download {
  wget -r --no-parent -A.deps $PERZL_WGET_LIST_DEPS
}

# INTERNAL USE -- prepare download
# oss4aix_perzl_deps_download
# output
#   create ./www.oss4aix.org.site.dirs
# One time operation:
#   db included download,
#   so customer need never run
#   (very, very loooooooong running task)
function oss4aix_perzl_subdir_download {
  # echo "ftp://www.oss4aix.org/RPMS/$1/"
  if (($system_OS400==1))
  then
    wget --dont-remove-listing --quiet  "ftp://www.oss4aix.org/RPMS/$1/"
  else
    wget --no-remove-listing --quiet  "ftp://www.oss4aix.org/RPMS/$1/"
  fi
  mv .listing child.listing
  while read rec
  do
    subdir=$(echo $rec | awk '{print $9}')
    cnt=$(echo $subdir | grep -c '.rpm')
    if (($cnt == 1))
    then
      echo "$1 $subdir"
      echo "$1 $subdir" >> $PERZL_LOCAL_COPY_RPMS
    fi
  done < "child.listing"
  rm child.listing
  rm index.ht*
}
function oss4aix_perzl_dirs_download {
  rm $PERZL_LOCAL_COPY_RPMS
  rm .listing
  rm index.ht*
  touch $PERZL_LOCAL_COPY_RPMS
  if (($system_OS400==1))
  then
    wget --dont-remove-listing --quiet  ftp://www.oss4aix.org/RPMS/
  else
    wget --no-remove-listing --quiet  ftp://www.oss4aix.org/RPMS/
  fi
  mv .listing root.listing
  while read rec
  do
    subdir=$(echo $rec | awk '{print $9}' | tr -d '\r')
    # echo "$subdir"
    cnt=$(echo $subdir | grep -c '\.')
    if (($cnt == 0))
    then
      oss4aix_perzl_subdir_download "$subdir"
    fi
  done < "root.listing"
  rm .listing
  rm index.ht*
  rm root.listing
}


# ################################################
# Main Menu
# ################################################
cnt1=$(echo $1 | grep -c $PERZL_RELEASE_AIX53)
cnt2=$(echo $1 | grep -c $PERZL_RELEASE_AIX61)
if (($cnt1==0)) && (($cnt2==0))
then
  help_user
else
  assign_global_vars $1
  case "$2" in
  "")
  help_user
  ;;
  "bigdeps")
  oss4aix_perzl_deps_download
  ;;
  "bigrpms")
  oss4aix_perzl_dirs_download
  ;;
  "count")
  oss4aix_list_count $3
  ;;
  "list")
  oss4aix_list_display $3
  ;;
  "wget1")
  oss4aix_single "1" $3
  ;;
  "wget")
  oss4aix_tree "1" $3
  oss4aix_single "1" $3
  ;;
  "wgetv")
  PERZL_DEBUG="1"
  oss4aix_tree "1" $3
  oss4aix_single "1" $3
  ;;
  "rpm1")
  oss4aix_single "2" $3
  oss4aix_fix_libiconv
  ;;
  "rpm")
  oss4aix_tree "2" $3
  oss4aix_single "2" $3
  oss4aix_fix_libiconv
  ;;
  "rpmv")
  PERZL_DEBUG="1"
  oss4aix_tree "2" $3
  oss4aix_single "2" $3
  ;;
  "fix")
  oss4aix_fix_libiconv
  ;;
  "admin")
  help_admin
  ;;
  *)
  help_user
  ;;
  esac
fi
