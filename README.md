#Summary
Most IBM i folks are now using binaries from [Perzl site](perzl.org), therefore a new `wwwperzl.sh` script was created to web download and/or install rpms and dependencies from Perzl site. Perzl site provides handy list of dependencies for rpms you may download ( deplists), `setup2.sh` below creates a local copy of Perzl *.deps tree for use with `wwwperzl.sh`. This information and scripts are offered “as is”, please check perzl.org License link for additional details.

#Files:

##Summary
```
http://www.oss4aix.org (perzl site compatible with IBM site)
wwwperzl.sh                   -- perzl download newer AIX *.rpms (V5R4+)
www.oss4aix.org               -- perzl copy *.deps
www.oss4aix.org.site.dirs     -- perzl copy RPMS/ structure (names only)
```

```
ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox (IBM site)
setup.sh                      -- setup rpm for use
rpm.rte                       -- aixtoolbox copy of rpm.rte
wget-1.9.1-1.aix5.1.ppc.rpm   -- aixtoolbox copy of wget
wwwinstall.sh                 -- aixtoolbox download older AIX *.rpms (V5R3)
```

**admin:**
```
wwwclean.sh                   -- destroy (complete delete/remove/clean)
                                 /QOpenSys/download
                                 /QOpenSys/opt/freeware
                                 /opt/freeware
                                 /usr/bin (freeware/bin symbolic links)
                                 /usr/lib (freeware/lib symbolic links)
                                 (start all over from scratch)
```

##setup.sh
```
call qp2term
$ mkdir /QOpenSys/download    -- make root download directory for *.rpms (one time)
$ cd /QOpenSys/download       -- change root directory for *rpms download/install
$ chmod +x setup.sh           -- make script executable (one time)
$ setup.sh                    -- install rpm.rte and wget.rpm (example version: wget-1.9.1-1.aix5.1ppc.rpm)
```

##wwwperzl.sh (V5R4+)
```
$ ./wwwperzl.sh
```
 
###customer
```
wwwperzl.sh aix53|aix61 option [scan]
required:
 aix53|aix61
optional:
 option [scan]
 help          - help customer
 admin         - admin help, PASE releases, perzl site copy functions, etc.
 list   [scan] - list possible downloads
 count  [scan] - count list possible downloads
 wget1  [scan] - wget from perzl
 wget   [scan] - wget plus depends from perzl
 wgetv  [scan] - wget plus depends from perzl (view action only)
 rpm1   [scan] - rpm install local (post wget)
 rpm    [scan] - rpm install plus depends (post wget)
 rpmv   [scan] - rpm install plus depends (view action only)
                (*)[scan] - wild card rpm scan [gcc*]
```

**pase->aix release:**
```
 --------------
 V5R3 -> aix52
 V5R4 -> aix53
 V6R1 -> aix53
 V7R1 -> aix61
```

**Example:**

```
$ ./wwwperzl.sh aix53 wgetv curl-7.27.0-1
wget http://www.oss4aix.org/download/RPMS/bash/bash-4.2-10.aix5.1.ppc.rpm
wget http://www.oss4aix.org/download/RPMS/gdbm/gdbm-1.10-1.aix5.1.ppc.rpm
rpm [gettext-0.10.40-8.aix5.2.ppc.rpm] previously downloaded
-rw-r--r--    1 adc      0           1074719 Nov 24 2011  gettext-0.10.40-8.aix5.2.ppc.rpm
wget http://www.oss4aix.org/download/RPMS/info/info-4.13a-2.aix5.1.ppc.rpm
wget http://www.oss4aix.org/download/RPMS/gcc/libgcc-4.7.2-1.aix5.3.ppc.rpm
wget http://www.oss4aix.org/download/RPMS/libiconv/libiconv-1.14-2.aix5.1.ppc.rpm
wget http://www.oss4aix.org/download/RPMS/libidn/libidn-1.25-1.aix5.1.ppc.rpm
wget http://www.oss4aix.org/download/RPMS/libssh2/libssh2-1.4.2-2.aix5.1.ppc.rpm
wget http://www.oss4aix.org/download/RPMS/openssl/openssl-1.0.1c-1.aix5.1.ppc.rpm
wget http://www.oss4aix.org/download/RPMS/perl/perl-5.8.8-2.aix5.1.ppc.rpm
wget http://www.oss4aix.org/download/RPMS/zlib/zlib-1.2.3-4.aix5.2.ppc.rpm
wget http://www.oss4aix.org/download/RPMS/openldap/openldap-2.4.23-0.3.aix5.1.ppc.rpm
rpm [curl-7.27.0-1.aix5.1.ppc.rpm] previously downloaded
-rw-r--r--    1 adc      0            742625 Aug 28 2012  curl-7.27.0-1.aix5.1.ppc.rpm
```


##wwwinstall.sh (V5R3)

```
$ cd /QOpenSys/download
$ chmod +x wwwinstall.sh        <-make executable
wwwinstall.sh has 4 possible operations each package (BASE example) ... 
$ wwwinstall.sh GetBase         <- wget and install rpms (requires IBM i allowed Internet access)
$ wwwinstall.sh GetBase_wget    <- wget rpms only (requires IBM i allowed Internet access)
$ wwwinstall.sh GetBase_install <- install rpms only for manual install RPM packages IBM i without Internet Access (see dark side below)
$ wwwinstall.sh GetBase_clean   <- remove xxx.rpm files from /QOpenSys/download, but not remove product (optional post install clean up)
$ wwwinstall.sh GetBase_erase   <- uninstall/erase rpm packages (risky)
```

#License
MIT.  See [`LICENSE`](https://bitbucket.org/litmis/ibmiperzl/src) file.