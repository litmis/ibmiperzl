#!/bin/sh
# ################################################
# Web get rpms from the AIX toolkit site
# ################################################
function do_GetBase_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/bash/bash-doc-3.0-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/bash/bash-3.0-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/bc/bc-1.06-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/bzip2/bzip2-1.0.2-4.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/findutils/findutils-4.1-3.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gettext/gettext-0.10.40-6.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/git/git-4.3.20-4.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/grep/grep-2.5.1-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gzip/gzip-1.2.4a-9.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/texinfo/info-4.6-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/less/less-382-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/lsof/lsof-4.61-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/lsof/lsof-4.61-3.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/lsof/lsof-4.61-4.aix5.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/mawk/mawk-1.3.3-8.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/patch/patch-2.5.4-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rpm/popt-1.7-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/prngd/prngd-0.9.29-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/readline/readline-4.3-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rpm/rpm-build-3.0.5-41.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rpm/rpm-3.0.5-41.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rxvt/rxvt-2.6.3-3.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/screen/screen-3.9.10-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/sed/sed-4.1.1-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/sharutils/sharutils-4.2.1-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/sudo/sudo-1.6.7p5-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/tar/tar-1.14-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/tcsh/tcsh-6.11-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/unzip/unzip-5.51-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/which/which-2.14-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/zip/zip-2.3-3.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/zsh/zsh-4.0.4-3.aix5.1.ppc.rpm 
}
function do_GetXbase_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/aalib/aalib-1.2-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/glib/glib-1.2.10-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gtkplus/gtkplus-1.2.10-4.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gtk-doc/gtk-doc-1.1-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gtk-engines/gtk-engines-0.12-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gtkhtml2/gtkhtml2-2.4.0-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libjpeg/libjpeg-6b-6.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libmng/libmng-1.0.3-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libpng/libpng-1.2.8-6.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libtiff/libtiff-3.6.1-4.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libungif/libungif-4.1.2-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/Xaw3d/Xaw3d-1.5-4.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/Xbae/Xbae-4.9.1-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xcursor/xcursor-1.0.2-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xfce/xfce-3.8.11-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xpm/xpm-3.4k-7.aix5.1.ppc.rpm 
}
function do_GetWbase_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/curl/curl-7.9.3-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/expat/expat-1.95.7-4.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/jade/jade-1.2.1-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/librep/librep-0.14-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libxml2/libxml2-2.6.21-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libxslt/libxslt-1.1.5-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/lynx/lynx-2.8.4-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/pcre/pcre-3.7-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/squid/squid-2.4.STABLE7-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/texinfo/texinfo-4.6-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/webmin/webmin-1.150-1.aix5.1.noarch.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/wget/wget-1.9.1-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/zlib/zlib-1.2.3-3.aix5.1.ppc.rpm 
}
function do_GetPropList_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libPropList/libPropList-0.10.1-1.aix4.3.ppc.rpm 
}
function do_GetCurses_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ncurses/ncurses-5.2-3.aix4.3.ppc.rpm 
}
function do_GetXedit_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/emacs/emacs-el-21.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/emacs/emacs-leim-21.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/emacs/emacs-nox-21.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/emacs/emacs-X11-21.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/emacs/emacs-21.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/hexedit/hexedit-1.2.2-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/vim/vim-common-6.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/vim/vim-enhanced-6.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/vim/vim-minimal-6.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/vim/vim-X11-6.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xemacs/xemacs-el-21.1.14-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xemacs/xemacs-info-21.1.14-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xemacs/xemacs-21.1.14-1.aix4.3.ppc.rpm 
}
function do_GetXimage_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/cairo/cairo-1.0.2-6.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gd/gd-progs-1.8.4-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gd/gd-1.8.4-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gimp/gimp-libgimp-1.2.2-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gimp/gimp-1.2.2-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ImageMagick/ImageMagick-5.4.2-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libungif/libungif-progs-4.1.2-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/transfig/transfig-3.2.3d-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xfig/xfig-3.2.3d-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xpaint/xpaint-2.6.1-3.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xrender/xrender-0.8.4-7.aix5.1.ppc.rpm 
}
function do_GetEterm_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/Eterm/Eterm-backgrounds-0.8.10-5.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/Eterm/Eterm-0.8.10-5.aix4.3.ppc.rpm 
}
function do_GetScript_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/tcltk/expect-5.42.1-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/perl/perl-5.8.2-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/python/python-docs-2.3.4-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/python/python-tools-2.3.4-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/python/python-2.3.4-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/slang/slang-1.4.4-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/tcltk/tcl-8.4.7-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/tcltk/tk-8.4.7-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/python/tkinter-2.3.4-3.aix5.1.ppc.rpm 
}
function do_GetGuile_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/guile/guile-1.4-3.aix4.3.ppc.rpm 
}
function do_GetVnc_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/vnc/vnc-3.3.3r2-6.aix5.1.ppc.rpm 
}
function do_GetFtp_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ftpcopy/ftpcopy-0.3.9-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ncftp/ncftp-3.1.1-3.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/proftpd/proftpd-1.2.8-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/wu-ftpd/wu-ftpd-2.6.2-6.aix5.1.ppc.rpm 
}
function do_GetFileMgmt_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rdist/rdist-6.1.5-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rsync/rsync-2.6.2-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/zoo/zoo-2.10-5.aix5.1.ppc.rpm 
}
function do_GetAtk_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/atk/atk-1.10.3-2.aix5.1.ppc.rpm 
}
function do_GetPostScript_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/a2ps/a2ps-4.13-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/enscript/enscript-1.6.1-3.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/mpage/mpage-2.5-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/trueprint/trueprint-5.3-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/urw-fonts/urw-fonts-2.0-1.aix4.3.noarch.rpm 
}
function do_GetGhostScript_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ghostscript-fonts/ghostscript-fonts-6.0-1.aix4.3.noarch.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ghostscript/ghostscript-5.50-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gv/gv-3.5.8-4.aix4.3.ppc.rpm 
}
function do_GetPdf_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xpdf/xpdf-1.00-1.aix4.3.ppc.rpm 
}
function do_GetMicrosoft_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/mtools/mtools-3.9.8-3.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/samba/samba-client-2.2.7-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/samba/samba-common-2.2.7-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/samba/samba-2.2.7-4.aix4.3.ppc.rpm 
}
function do_GetMySql_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/MySQL/MySQL-client-3.23.58-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/MySQL/MySQL-3.23.58-2.aix5.1.ppc.rpm 
}
function do_GetDb_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/db/db-3.3.11-4.aix5.1.ppc.rpm 
}
function do_GetGdbm_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gdbm/gdbm-1.8.3-2.aix5.1.ppc.rpm 
}
function do_GetMedia_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/audiofile/audiofile-0.2.5-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/esound/esound-0.2.34-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xmms/xmms-1.2.7-1.aix4.3.ppc.rpm 
}
function do_GetCd_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/cdrecord/cdda2wav-1.9-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/cdrecord/cdrecord-1.9-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/cdrecord/cdrecord-1.9-7.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/cdrecord/mkisofs-1.13-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xmcd/xmcd-3.0.2-1.aix4.3.ppc.rpm 
}
function do_GetSpell_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/aspell/aspell-0.33.6.3-5.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/pspell/pspell-0.12.2-2.aix4.3.ppc.rpm 
}
function do_DevSpell_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/aspell/aspell-devel-0.33.6.3-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/pspell/pspell-devel-0.12.2-2.aix4.3.ppc.rpm 
}
function do_GetCal_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcal/gcal-3.01-1.aix4.3.ppc.rpm 
}
function do_GetGraph_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gnuplot/gnuplot-3.7.1-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/plotutils/plotutils-2.4.1-1.aix4.3.ppc.rpm 
}
function do_GetGroff_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/groff/groff-gxditview-1.17.2-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/groff/groff-1.17.2-1.aix4.3.ppc.rpm 
}
function do_GetPhp_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/php/php-manual-4.0.6-5.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/php/php-4.0.6-5.aix4.3.ppc.rpm 
}
function do_GetApache_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/apache/apache-manual-1.3.31-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/apache/apache-1.3.31-1.aix5.1.ppc.rpm 
}
function do_GetChat_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gaim/gaim-0.52-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/jabber/jabber-1.4.2-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ytalk/ytalk-3.1.1-2.aix4.3.ppc.rpm 
}
function do_GetMail_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/elm/elm-2.5.6-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/fetchmail/fetchmail-5.9.10-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/fetchmail/fetchmailconf-5.9.10-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/metamail/metamail-2.7-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/mutt/mutt-1.4.2.1-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/pine/pine-4.44-3.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/procmail/procmail-3.21-1.aix4.3.ppc.rpm 
}
function do_GetFont_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/fnlib/fnlib-0.5-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/fontconfig/fontconfig-2.2.2-6.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/freetype/freetype-demo-1.3.1-9.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/freetype/freetype-1.3.1-9.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xft/xft-2.1.6-5.aix5.1.ppc.rpm 
}
function do_DevFont_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/fnlib/fnlib-devel-0.5-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/fontconfig/fontconfig-devel-2.2.2-6.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/freetype/freetype-devel-1.3.1-9.aix5.1.ppc.rpm 
}
function do_GetFont2_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/freetype2/freetype2-2.1.7-5.aix5.1.ppc.rpm 
}
function do_DevFont2_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/freetype2/freetype2-devel-2.1.7-5.aix5.1.ppc.rpm 
}
function do_GetXbase2_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/glib2/glib2-2.8.1-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gtk2-engines/gtk2-engines-2.2.0-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gtk2/gtk2-2.8.3-9.aix5.1.ppc.rpm 
}
function do_DevXbase2_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/glib2/glib2-devel-2.8.1-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gtk2/gtk2-devel-2.8.3-9.aix5.1.ppc.rpm 
}
function do_GetCimom_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/openCIMOM/openCIMOM-0.7-4.aix5.1.noarch.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/openCIMOM/openCIMOM-0.8-1.aix5.2.noarch.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/pegasus/pegasus-1.0-9.aix5.1.ppc.rpm 
}
function do_GetModem_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/lrzsz/lrzsz-0.12.20-2.aix4.3.ppc.rpm 
}
function do_GetCompileTools_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/autoconf213/autoconf213-2.13-1.aix5.1.noarch.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/autoconf/autoconf-2.59-1.aix5.1.noarch.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/automake/automake-1.8.5-1.aix5.1.noarch.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/binutils/binutils-2.14-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/bison/bison-1.875-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/coreutils/coreutils-5.2.1-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/cpio/cpio-2.5-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ddd/ddd-3.3.1-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/diffutils/diffutils-2.8.1-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ElectricFence/ElectricFence-2.2.2-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/flex/flex-2.5.4a-6.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gawk/gawk-3.1.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/gcc-cplusplus-4.0.0-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/gcc-cplusplus-4.0.0-1.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/gcc-cplusplus-4.0.0-1.aix5.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/gcc-4.0.0-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/gcc-4.0.0-1.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/gcc-4.0.0-1.aix5.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gdb/gdb-6.0-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/help2man/help2man-1.29-1.aix4.3.noarch.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/indent/indent-2.2.7-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/intltool/intltool-0.27.2-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/libgcc-4.0.0-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/libgcc-4.0.0-1.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/libgcc-4.0.0-1.aix5.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/libstdcplusplus-4.0.0-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/libstdcplusplus-4.0.0-1.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/libstdcplusplus-4.0.0-1.aix5.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libtool/libtool-1.5.8-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/m4/m4-1.4.1-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/make/make-3.80-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/pkg-config/pkg-config-0.19-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/pth/pth-1.4.0-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/qt/qt-designer-3.0.3-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/qt/qt-Xt-3.0.3-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/qt/qt-3.0.3-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/smake/smake-1.3.2-1.aix4.3.noarch.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/splint/splint-3.0.1.6-2.aix5.1.ppc.rpm 
}
function do_GetCvs_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/cvs/cvs-1.11.17-3.aix5.1.ppc.rpm 
}
function do_GetRcs_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rcs/rcs-5.7-2.aix5.1.ppc.rpm 
}
function do_GetTcpTools_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ethereal/ethereal-0.8.18-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libpcap/libpcap-0.8.3-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/tcpdump/tcpdump-3.8.1-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/traceroute/traceroute-1.4a12-2.aix4.3.ppc.rpm 
}
function do_GetDocbook_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/docbookx/docbookx-4.1.2-2.aix5.1.noarch.rpm 
}
function do_GetDejagnu_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/dejagnu/dejagnu-1.4.2-1.aix4.3.ppc.rpm 
}
function do_DevCompileTools_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/libstdcplusplus-devel-4.0.0-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/libstdcplusplus-devel-4.0.0-1.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gcc/libstdcplusplus-devel-4.0.0-1.aix5.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/pth/pth-devel-1.4.0-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/qt/qt-devel-3.0.3-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/readline/readline-devel-4.3-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rpm/rpm-devel-3.0.5-41.aix5.1.ppc.rpm 
}
function do_DevXbase_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/aalib/aalib-devel-1.2-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/glib/glib-devel-1.2.10-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gtkplus/gtkplus-devel-1.2.10-4.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gtkhtml2/gtkhtml2-devel-2.4.0-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/imlib/imlib-devel-1.9.11-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libjpeg/libjpeg-devel-6b-6.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libmng/libmng-devel-1.0.3-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libpng/libpng-devel-1.2.8-6.aix5.2.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libtiff/libtiff-devel-3.6.1-4.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libungif/libungif-devel-4.1.2-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/Xaw3d/Xaw3d-devel-1.5-4.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/Xbae/Xbae-devel-4.9.1-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xcursor/xcursor-devel-1.0.2-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xpm/xpm-devel-3.4k-7.aix5.1.ppc.rpm 
}
function do_DevXimage_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/cairo/cairo-devel-1.0.2-6.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gd/gd-devel-1.8.4-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gimp/gimp-devel-1.2.2-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ImageMagick/ImageMagick-devel-5.4.2-3.aix5.1.ppc.rpm 
}
function do_DevWbase_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/curl/curl-devel-7.9.3-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/expat/expat-devel-1.95.7-4.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/librep/librep-devel-0.14-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libxml2/libxml2-devel-2.6.21-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/libxslt/libxslt-devel-1.1.5-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/pcre/pcre-devel-3.7-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/zlib/zlib-devel-1.2.3-3.aix5.1.ppc.rpm 
}
function do_DevScript_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/python/python-devel-2.3.4-2.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/slang/slang-devel-1.4.4-2.aix4.3.ppc.rpm 
}
function do_DevMedia_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/audiofile/audiofile-devel-0.2.5-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/esound/esound-devel-0.2.34-1.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xmms/xmms-devel-1.2.7-1.aix4.3.ppc.rpm 
}
function do_DevGdbm_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gdbm/gdbm-devel-1.8.3-2.aix5.1.ppc.rpm 
}
function do_DevGuile_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/guile/guile-devel-1.4-3.aix4.3.ppc.rpm 
}
function do_DevCurses_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/ncurses/ncurses-devel-5.2-3.aix4.3.ppc.rpm 
}
function do_DevPhp_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/php/php-devel-4.0.6-5.aix4.3.ppc.rpm 
}
function do_DevApache_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/apache/apache-devel-1.3.31-1.aix5.1.ppc.rpm 
}
function do_DevAtk_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/atk/atk-devel-1.10.3-2.aix5.1.ppc.rpm 
}
function do_DevCd_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/cdrecord/cdrecord-devel-1.9-4.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/cdrecord/cdrecord-devel-1.9-7.aix5.2.ppc.rpm 
}
function do_DevMySql_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/MySQL/MySQL-devel-3.23.58-2.aix5.1.ppc.rpm 
}
function do_GetGames_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/gnuchess/gnuchess-4.0.pl80-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xboard/xboard-4.2.6-1.aix4.3.ppc.rpm 
}
function do_GetDeskTop_wget { 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/AfterStep/AfterStep-1.8.10-1.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/enlightenment/enlightenment-0.16.5-3.aix5.1.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/icewm/icewm-1.0.9-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/WindowMaker/WindowMaker-0.65.1-2.aix4.3.ppc.rpm 
    wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/xscreensaver/xscreensaver-4.06-2.aix5.1.ppc.rpm 
}
# ################################################
# Install rpms from this directory
# ################################################
function do_GetBase_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv bash-doc-3.0-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv bash-3.0-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv bc-1.06-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv bzip2-1.0.2-4.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv findutils-4.1-3.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gettext-0.10.40-6.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv git-4.3.20-4.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv grep-2.5.1-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gzip-1.2.4a-9.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv info-4.6-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv less-382-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv lsof-4.61-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv lsof-4.61-3.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv lsof-4.61-4.aix5.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv mawk-1.3.3-8.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv patch-2.5.4-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv popt-1.7-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv prngd-0.9.29-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv readline-4.3-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv rpm-build-3.0.5-41.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv rpm-3.0.5-41.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv rxvt-2.6.3-3.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv screen-3.9.10-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv sed-4.1.1-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv sharutils-4.2.1-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv sudo-1.6.7p5-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv tar-1.14-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv tcsh-6.11-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv unzip-5.51-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv which-2.14-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv zip-2.3-3.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv zsh-4.0.4-3.aix5.1.ppc.rpm 
}
function do_GetXbase_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv aalib-1.2-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv glib-1.2.10-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gtkplus-1.2.10-4.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gtk-doc-1.1-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gtk-engines-0.12-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gtkhtml2-2.4.0-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libjpeg-6b-6.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libmng-1.0.3-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libpng-1.2.8-6.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libtiff-3.6.1-4.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libungif-4.1.2-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv Xaw3d-1.5-4.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv Xbae-4.9.1-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xcursor-1.0.2-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xfce-3.8.11-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xpm-3.4k-7.aix5.1.ppc.rpm 
}
function do_GetWbase_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv curl-7.9.3-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv expat-1.95.7-4.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv jade-1.2.1-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv librep-0.14-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libxml2-2.6.21-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libxslt-1.1.5-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv lynx-2.8.4-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv pcre-3.7-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv squid-2.4.STABLE7-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv texinfo-4.6-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv webmin-1.150-1.aix5.1.noarch.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv wget-1.9.1-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv zlib-1.2.3-3.aix5.1.ppc.rpm 
}
function do_GetPropList_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libPropList-0.10.1-1.aix4.3.ppc.rpm 
}
function do_GetCurses_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ncurses-5.2-3.aix4.3.ppc.rpm 
}
function do_GetXedit_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv emacs-el-21.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv emacs-leim-21.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv emacs-nox-21.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv emacs-X11-21.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv emacs-21.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv hexedit-1.2.2-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv vim-common-6.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv vim-enhanced-6.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv vim-minimal-6.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv vim-X11-6.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xemacs-el-21.1.14-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xemacs-info-21.1.14-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xemacs-21.1.14-1.aix4.3.ppc.rpm 
}
function do_GetXimage_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv cairo-1.0.2-6.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gd-progs-1.8.4-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gd-1.8.4-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gimp-libgimp-1.2.2-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gimp-1.2.2-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ImageMagick-5.4.2-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libungif-progs-4.1.2-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv transfig-3.2.3d-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xfig-3.2.3d-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xpaint-2.6.1-3.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xrender-0.8.4-7.aix5.1.ppc.rpm 
}
function do_GetEterm_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv Eterm-backgrounds-0.8.10-5.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv Eterm-0.8.10-5.aix4.3.ppc.rpm 
}
function do_GetScript_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv expect-5.42.1-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv perl-5.8.2-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv python-docs-2.3.4-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv python-tools-2.3.4-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv python-2.3.4-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv slang-1.4.4-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv tcl-8.4.7-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv tk-8.4.7-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv tkinter-2.3.4-3.aix5.1.ppc.rpm 
}
function do_GetGuile_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv guile-1.4-3.aix4.3.ppc.rpm 
}
function do_GetVnc_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv vnc-3.3.3r2-6.aix5.1.ppc.rpm 
}
function do_GetFtp_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ftpcopy-0.3.9-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ncftp-3.1.1-3.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv proftpd-1.2.8-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv wu-ftpd-2.6.2-6.aix5.1.ppc.rpm 
}
function do_GetFileMgmt_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv rdist-6.1.5-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv rsync-2.6.2-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv zoo-2.10-5.aix5.1.ppc.rpm 
}
function do_GetAtk_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv atk-1.10.3-2.aix5.1.ppc.rpm 
}
function do_GetPostScript_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv a2ps-4.13-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv enscript-1.6.1-3.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv mpage-2.5-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv trueprint-5.3-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv urw-fonts-2.0-1.aix4.3.noarch.rpm 
}
function do_GetGhostScript_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ghostscript-fonts-6.0-1.aix4.3.noarch.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ghostscript-5.50-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gv-3.5.8-4.aix4.3.ppc.rpm 
}
function do_GetPdf_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xpdf-1.00-1.aix4.3.ppc.rpm 
}
function do_GetMicrosoft_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv mtools-3.9.8-3.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv samba-client-2.2.7-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv samba-common-2.2.7-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv samba-2.2.7-4.aix4.3.ppc.rpm 
}
function do_GetMySql_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv MySQL-client-3.23.58-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv MySQL-3.23.58-2.aix5.1.ppc.rpm 
}
function do_GetDb_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv db-3.3.11-4.aix5.1.ppc.rpm 
}
function do_GetGdbm_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gdbm-1.8.3-2.aix5.1.ppc.rpm 
}
function do_GetMedia_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv audiofile-0.2.5-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv esound-0.2.34-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xmms-1.2.7-1.aix4.3.ppc.rpm 
}
function do_GetCd_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv cdda2wav-1.9-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv cdrecord-1.9-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv cdrecord-1.9-7.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv mkisofs-1.13-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xmcd-3.0.2-1.aix4.3.ppc.rpm 
}
function do_GetSpell_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv aspell-0.33.6.3-5.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv pspell-0.12.2-2.aix4.3.ppc.rpm 
}
function do_DevSpell_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv aspell-devel-0.33.6.3-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv pspell-devel-0.12.2-2.aix4.3.ppc.rpm 
}
function do_GetCal_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gcal-3.01-1.aix4.3.ppc.rpm 
}
function do_GetGraph_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gnuplot-3.7.1-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv plotutils-2.4.1-1.aix4.3.ppc.rpm 
}
function do_GetGroff_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv groff-gxditview-1.17.2-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv groff-1.17.2-1.aix4.3.ppc.rpm 
}
function do_GetPhp_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv php-manual-4.0.6-5.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv php-4.0.6-5.aix4.3.ppc.rpm 
}
function do_GetApache_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv apache-manual-1.3.31-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv apache-1.3.31-1.aix5.1.ppc.rpm 
}
function do_GetChat_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gaim-0.52-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv jabber-1.4.2-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ytalk-3.1.1-2.aix4.3.ppc.rpm 
}
function do_GetMail_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv elm-2.5.6-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv fetchmail-5.9.10-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv fetchmailconf-5.9.10-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv metamail-2.7-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv mutt-1.4.2.1-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv pine-4.44-3.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv procmail-3.21-1.aix4.3.ppc.rpm 
}
function do_GetFont_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv fnlib-0.5-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv fontconfig-2.2.2-6.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv freetype-demo-1.3.1-9.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv freetype-1.3.1-9.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xft-2.1.6-5.aix5.1.ppc.rpm 
}
function do_DevFont_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv fnlib-devel-0.5-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv fontconfig-devel-2.2.2-6.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv freetype-devel-1.3.1-9.aix5.1.ppc.rpm 
}
function do_GetFont2_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv freetype2-2.1.7-5.aix5.1.ppc.rpm 
}
function do_DevFont2_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv freetype2-devel-2.1.7-5.aix5.1.ppc.rpm 
}
function do_GetXbase2_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv glib2-2.8.1-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gtk2-engines-2.2.0-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gtk2-2.8.3-9.aix5.1.ppc.rpm 
}
function do_DevXbase2_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv glib2-devel-2.8.1-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gtk2-devel-2.8.3-9.aix5.1.ppc.rpm 
}
function do_GetCimom_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv openCIMOM-0.7-4.aix5.1.noarch.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv openCIMOM-0.8-1.aix5.2.noarch.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv pegasus-1.0-9.aix5.1.ppc.rpm 
}
function do_GetModem_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv lrzsz-0.12.20-2.aix4.3.ppc.rpm 
}
function do_GetCompileTools_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv autoconf213-2.13-1.aix5.1.noarch.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv autoconf-2.59-1.aix5.1.noarch.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv automake-1.8.5-1.aix5.1.noarch.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv binutils-2.14-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv bison-1.875-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv coreutils-5.2.1-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv cpio-2.5-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ddd-3.3.1-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv diffutils-2.8.1-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ElectricFence-2.2.2-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv flex-2.5.4a-6.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gawk-3.1.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gcc-cplusplus-4.0.0-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gcc-cplusplus-4.0.0-1.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gcc-cplusplus-4.0.0-1.aix5.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gcc-4.0.0-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gcc-4.0.0-1.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gcc-4.0.0-1.aix5.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gdb-6.0-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv help2man-1.29-1.aix4.3.noarch.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv indent-2.2.7-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv intltool-0.27.2-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libgcc-4.0.0-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libgcc-4.0.0-1.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libgcc-4.0.0-1.aix5.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libstdcplusplus-4.0.0-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libstdcplusplus-4.0.0-1.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libstdcplusplus-4.0.0-1.aix5.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libtool-1.5.8-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv m4-1.4.1-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv make-3.80-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv pkg-config-0.19-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv pth-1.4.0-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv qt-designer-3.0.3-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv qt-Xt-3.0.3-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv qt-3.0.3-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv smake-1.3.2-1.aix4.3.noarch.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv splint-3.0.1.6-2.aix5.1.ppc.rpm 
}
function do_GetCvs_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv cvs-1.11.17-3.aix5.1.ppc.rpm 
}
function do_GetRcs_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv rcs-5.7-2.aix5.1.ppc.rpm 
}
function do_GetTcpTools_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ethereal-0.8.18-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libpcap-0.8.3-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv tcpdump-3.8.1-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv traceroute-1.4a12-2.aix4.3.ppc.rpm 
}
function do_GetDocbook_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv docbookx-4.1.2-2.aix5.1.noarch.rpm 
}
function do_GetDejagnu_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv dejagnu-1.4.2-1.aix4.3.ppc.rpm 
}
function do_DevCompileTools_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libstdcplusplus-devel-4.0.0-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libstdcplusplus-devel-4.0.0-1.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libstdcplusplus-devel-4.0.0-1.aix5.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv pth-devel-1.4.0-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv qt-devel-3.0.3-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv readline-devel-4.3-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv rpm-devel-3.0.5-41.aix5.1.ppc.rpm 
}
function do_DevXbase_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv aalib-devel-1.2-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv glib-devel-1.2.10-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gtkplus-devel-1.2.10-4.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gtkhtml2-devel-2.4.0-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv imlib-devel-1.9.11-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libjpeg-devel-6b-6.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libmng-devel-1.0.3-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libpng-devel-1.2.8-6.aix5.2.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libtiff-devel-3.6.1-4.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libungif-devel-4.1.2-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv Xaw3d-devel-1.5-4.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv Xbae-devel-4.9.1-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xcursor-devel-1.0.2-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xpm-devel-3.4k-7.aix5.1.ppc.rpm 
}
function do_DevXimage_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv cairo-devel-1.0.2-6.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gd-devel-1.8.4-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gimp-devel-1.2.2-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ImageMagick-devel-5.4.2-3.aix5.1.ppc.rpm 
}
function do_DevWbase_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv curl-devel-7.9.3-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv expat-devel-1.95.7-4.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv librep-devel-0.14-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libxml2-devel-2.6.21-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv libxslt-devel-1.1.5-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv pcre-devel-3.7-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv zlib-devel-1.2.3-3.aix5.1.ppc.rpm 
}
function do_DevScript_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv python-devel-2.3.4-2.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv slang-devel-1.4.4-2.aix4.3.ppc.rpm 
}
function do_DevMedia_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv audiofile-devel-0.2.5-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv esound-devel-0.2.34-1.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xmms-devel-1.2.7-1.aix4.3.ppc.rpm 
}
function do_DevGdbm_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gdbm-devel-1.8.3-2.aix5.1.ppc.rpm 
}
function do_DevGuile_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv guile-devel-1.4-3.aix4.3.ppc.rpm 
}
function do_DevCurses_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv ncurses-devel-5.2-3.aix4.3.ppc.rpm 
}
function do_DevPhp_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv php-devel-4.0.6-5.aix4.3.ppc.rpm 
}
function do_DevApache_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv apache-devel-1.3.31-1.aix5.1.ppc.rpm 
}
function do_DevAtk_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv atk-devel-1.10.3-2.aix5.1.ppc.rpm 
}
function do_DevCd_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv cdrecord-devel-1.9-4.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv cdrecord-devel-1.9-7.aix5.2.ppc.rpm 
}
function do_DevMySql_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv MySQL-devel-3.23.58-2.aix5.1.ppc.rpm 
}
function do_GetGames_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv gnuchess-4.0.pl80-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xboard-4.2.6-1.aix4.3.ppc.rpm 
}
function do_GetDeskTop_install { 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv AfterStep-1.8.10-1.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv enlightenment-0.16.5-3.aix5.1.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv icewm-1.0.9-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv WindowMaker-0.65.1-2.aix4.3.ppc.rpm 
  rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv xscreensaver-4.06-2.aix5.1.ppc.rpm 
}
# ################################################
# Clean up old rpm files from wget in this dir
# ################################################
function do_GetBase_clean { 
  if test -e bash-doc-3.0-1.aix5.1.ppc.rpm ; then
    rm bash-doc-3.0-1.aix5.1.ppc.rpm 
  fi
  if test -e bash-3.0-1.aix5.1.ppc.rpm ; then
    rm bash-3.0-1.aix5.1.ppc.rpm 
  fi
  if test -e bc-1.06-2.aix4.3.ppc.rpm ; then
    rm bc-1.06-2.aix4.3.ppc.rpm 
  fi
  if test -e bzip2-1.0.2-4.aix5.1.ppc.rpm ; then
    rm bzip2-1.0.2-4.aix5.1.ppc.rpm 
  fi
  if test -e findutils-4.1-3.aix4.3.ppc.rpm ; then
    rm findutils-4.1-3.aix4.3.ppc.rpm 
  fi
  if test -e gettext-0.10.40-6.aix5.1.ppc.rpm ; then
    rm gettext-0.10.40-6.aix5.1.ppc.rpm 
  fi
  if test -e git-4.3.20-4.aix5.1.ppc.rpm ; then
    rm git-4.3.20-4.aix5.1.ppc.rpm 
  fi
  if test -e grep-2.5.1-1.aix4.3.ppc.rpm ; then
    rm grep-2.5.1-1.aix4.3.ppc.rpm 
  fi
  if test -e gzip-1.2.4a-9.aix5.1.ppc.rpm ; then
    rm gzip-1.2.4a-9.aix5.1.ppc.rpm 
  fi
  if test -e info-4.6-1.aix5.1.ppc.rpm ; then
    rm info-4.6-1.aix5.1.ppc.rpm 
  fi
  if test -e less-382-1.aix5.1.ppc.rpm ; then
    rm less-382-1.aix5.1.ppc.rpm 
  fi
  if test -e lsof-4.61-3.aix5.1.ppc.rpm ; then
    rm lsof-4.61-3.aix5.1.ppc.rpm 
  fi
  if test -e lsof-4.61-3.aix5.2.ppc.rpm ; then
    rm lsof-4.61-3.aix5.2.ppc.rpm 
  fi
  if test -e lsof-4.61-4.aix5.3.ppc.rpm ; then
    rm lsof-4.61-4.aix5.3.ppc.rpm 
  fi
  if test -e mawk-1.3.3-8.aix4.3.ppc.rpm ; then
    rm mawk-1.3.3-8.aix4.3.ppc.rpm 
  fi
  if test -e patch-2.5.4-4.aix4.3.ppc.rpm ; then
    rm patch-2.5.4-4.aix4.3.ppc.rpm 
  fi
  if test -e popt-1.7-2.aix5.1.ppc.rpm ; then
    rm popt-1.7-2.aix5.1.ppc.rpm 
  fi
  if test -e prngd-0.9.29-1.aix5.1.ppc.rpm ; then
    rm prngd-0.9.29-1.aix5.1.ppc.rpm 
  fi
  if test -e readline-4.3-2.aix5.1.ppc.rpm ; then
    rm readline-4.3-2.aix5.1.ppc.rpm 
  fi
  if test -e rpm-build-3.0.5-41.aix5.1.ppc.rpm ; then
    rm rpm-build-3.0.5-41.aix5.1.ppc.rpm 
  fi
  if test -e rpm-3.0.5-41.aix5.1.ppc.rpm ; then
    rm rpm-3.0.5-41.aix5.1.ppc.rpm 
  fi
  if test -e rxvt-2.6.3-3.aix4.3.ppc.rpm ; then
    rm rxvt-2.6.3-3.aix4.3.ppc.rpm 
  fi
  if test -e screen-3.9.10-2.aix4.3.ppc.rpm ; then
    rm screen-3.9.10-2.aix4.3.ppc.rpm 
  fi
  if test -e sed-4.1.1-1.aix5.1.ppc.rpm ; then
    rm sed-4.1.1-1.aix5.1.ppc.rpm 
  fi
  if test -e sharutils-4.2.1-1.aix4.3.ppc.rpm ; then
    rm sharutils-4.2.1-1.aix4.3.ppc.rpm 
  fi
  if test -e sudo-1.6.7p5-3.aix5.1.ppc.rpm ; then
    rm sudo-1.6.7p5-3.aix5.1.ppc.rpm 
  fi
  if test -e tar-1.14-2.aix5.1.ppc.rpm ; then
    rm tar-1.14-2.aix5.1.ppc.rpm 
  fi
  if test -e tcsh-6.11-3.aix5.1.ppc.rpm ; then
    rm tcsh-6.11-3.aix5.1.ppc.rpm 
  fi
  if test -e unzip-5.51-1.aix5.1.ppc.rpm ; then
    rm unzip-5.51-1.aix5.1.ppc.rpm 
  fi
  if test -e which-2.14-1.aix5.1.ppc.rpm ; then
    rm which-2.14-1.aix5.1.ppc.rpm 
  fi
  if test -e zip-2.3-3.aix4.3.ppc.rpm ; then
    rm zip-2.3-3.aix4.3.ppc.rpm 
  fi
  if test -e zsh-4.0.4-3.aix5.1.ppc.rpm ; then
    rm zsh-4.0.4-3.aix5.1.ppc.rpm 
  fi
}
function do_GetXbase_clean { 
  if test -e aalib-1.2-1.aix4.3.ppc.rpm ; then
    rm aalib-1.2-1.aix4.3.ppc.rpm 
  fi
  if test -e glib-1.2.10-2.aix4.3.ppc.rpm ; then
    rm glib-1.2.10-2.aix4.3.ppc.rpm 
  fi
  if test -e gtkplus-1.2.10-4.aix5.1.ppc.rpm ; then
    rm gtkplus-1.2.10-4.aix5.1.ppc.rpm 
  fi
  if test -e gtk-doc-1.1-2.aix5.1.ppc.rpm ; then
    rm gtk-doc-1.1-2.aix5.1.ppc.rpm 
  fi
  if test -e gtk-engines-0.12-1.aix4.3.ppc.rpm ; then
    rm gtk-engines-0.12-1.aix4.3.ppc.rpm 
  fi
  if test -e gtkhtml2-2.4.0-2.aix5.1.ppc.rpm ; then
    rm gtkhtml2-2.4.0-2.aix5.1.ppc.rpm 
  fi
  if test -e libjpeg-6b-6.aix5.1.ppc.rpm ; then
    rm libjpeg-6b-6.aix5.1.ppc.rpm 
  fi
  if test -e libmng-1.0.3-2.aix4.3.ppc.rpm ; then
    rm libmng-1.0.3-2.aix4.3.ppc.rpm 
  fi
  if test -e libpng-1.2.8-6.aix5.2.ppc.rpm ; then
    rm libpng-1.2.8-6.aix5.2.ppc.rpm 
  fi
  if test -e libtiff-3.6.1-4.aix5.1.ppc.rpm ; then
    rm libtiff-3.6.1-4.aix5.1.ppc.rpm 
  fi
  if test -e libungif-4.1.2-1.aix5.1.ppc.rpm ; then
    rm libungif-4.1.2-1.aix5.1.ppc.rpm 
  fi
  if test -e Xaw3d-1.5-4.aix5.1.ppc.rpm ; then
    rm Xaw3d-1.5-4.aix5.1.ppc.rpm 
  fi
  if test -e Xbae-4.9.1-2.aix4.3.ppc.rpm ; then
    rm Xbae-4.9.1-2.aix4.3.ppc.rpm 
  fi
  if test -e xcursor-1.0.2-3.aix5.1.ppc.rpm ; then
    rm xcursor-1.0.2-3.aix5.1.ppc.rpm 
  fi
  if test -e xfce-3.8.11-1.aix4.3.ppc.rpm ; then
    rm xfce-3.8.11-1.aix4.3.ppc.rpm 
  fi
  if test -e xpm-3.4k-7.aix5.1.ppc.rpm ; then
    rm xpm-3.4k-7.aix5.1.ppc.rpm 
  fi
}
function do_GetWbase_clean { 
  if test -e curl-7.9.3-2.aix4.3.ppc.rpm ; then
    rm curl-7.9.3-2.aix4.3.ppc.rpm 
  fi
  if test -e expat-1.95.7-4.aix5.1.ppc.rpm ; then
    rm expat-1.95.7-4.aix5.1.ppc.rpm 
  fi
  if test -e jade-1.2.1-4.aix4.3.ppc.rpm ; then
    rm jade-1.2.1-4.aix4.3.ppc.rpm 
  fi
  if test -e librep-0.14-1.aix4.3.ppc.rpm ; then
    rm librep-0.14-1.aix4.3.ppc.rpm 
  fi
  if test -e libxml2-2.6.21-2.aix5.1.ppc.rpm ; then
    rm libxml2-2.6.21-2.aix5.1.ppc.rpm 
  fi
  if test -e libxslt-1.1.5-2.aix5.1.ppc.rpm ; then
    rm libxslt-1.1.5-2.aix5.1.ppc.rpm 
  fi
  if test -e lynx-2.8.4-2.aix5.1.ppc.rpm ; then
    rm lynx-2.8.4-2.aix5.1.ppc.rpm 
  fi
  if test -e pcre-3.7-3.aix5.1.ppc.rpm ; then
    rm pcre-3.7-3.aix5.1.ppc.rpm 
  fi
  if test -e squid-2.4.STABLE7-2.aix4.3.ppc.rpm ; then
    rm squid-2.4.STABLE7-2.aix4.3.ppc.rpm 
  fi
  if test -e texinfo-4.6-1.aix5.1.ppc.rpm ; then
    rm texinfo-4.6-1.aix5.1.ppc.rpm 
  fi
  if test -e webmin-1.150-1.aix5.1.noarch.rpm ; then
    rm webmin-1.150-1.aix5.1.noarch.rpm 
  fi
  if test -e wget-1.9.1-1.aix5.1.ppc.rpm ; then
    rm wget-1.9.1-1.aix5.1.ppc.rpm 
  fi
  if test -e zlib-1.2.3-3.aix5.1.ppc.rpm ; then
    rm zlib-1.2.3-3.aix5.1.ppc.rpm 
  fi
}
function do_GetPropList_clean { 
  if test -e libPropList-0.10.1-1.aix4.3.ppc.rpm ; then
    rm libPropList-0.10.1-1.aix4.3.ppc.rpm 
  fi
}
function do_GetCurses_clean { 
  if test -e ncurses-5.2-3.aix4.3.ppc.rpm ; then
    rm ncurses-5.2-3.aix4.3.ppc.rpm 
  fi
}
function do_GetXedit_clean { 
  if test -e emacs-el-21.3-1.aix5.1.ppc.rpm ; then
    rm emacs-el-21.3-1.aix5.1.ppc.rpm 
  fi
  if test -e emacs-leim-21.3-1.aix5.1.ppc.rpm ; then
    rm emacs-leim-21.3-1.aix5.1.ppc.rpm 
  fi
  if test -e emacs-nox-21.3-1.aix5.1.ppc.rpm ; then
    rm emacs-nox-21.3-1.aix5.1.ppc.rpm 
  fi
  if test -e emacs-X11-21.3-1.aix5.1.ppc.rpm ; then
    rm emacs-X11-21.3-1.aix5.1.ppc.rpm 
  fi
  if test -e emacs-21.3-1.aix5.1.ppc.rpm ; then
    rm emacs-21.3-1.aix5.1.ppc.rpm 
  fi
  if test -e hexedit-1.2.2-1.aix4.3.ppc.rpm ; then
    rm hexedit-1.2.2-1.aix4.3.ppc.rpm 
  fi
  if test -e vim-common-6.3-1.aix5.1.ppc.rpm ; then
    rm vim-common-6.3-1.aix5.1.ppc.rpm 
  fi
  if test -e vim-enhanced-6.3-1.aix5.1.ppc.rpm ; then
    rm vim-enhanced-6.3-1.aix5.1.ppc.rpm 
  fi
  if test -e vim-minimal-6.3-1.aix5.1.ppc.rpm ; then
    rm vim-minimal-6.3-1.aix5.1.ppc.rpm 
  fi
  if test -e vim-X11-6.3-1.aix5.1.ppc.rpm ; then
    rm vim-X11-6.3-1.aix5.1.ppc.rpm 
  fi
  if test -e xemacs-el-21.1.14-1.aix4.3.ppc.rpm ; then
    rm xemacs-el-21.1.14-1.aix4.3.ppc.rpm 
  fi
  if test -e xemacs-info-21.1.14-1.aix4.3.ppc.rpm ; then
    rm xemacs-info-21.1.14-1.aix4.3.ppc.rpm 
  fi
  if test -e xemacs-21.1.14-1.aix4.3.ppc.rpm ; then
    rm xemacs-21.1.14-1.aix4.3.ppc.rpm 
  fi
}
function do_GetXimage_clean { 
  if test -e cairo-1.0.2-6.aix5.1.ppc.rpm ; then
    rm cairo-1.0.2-6.aix5.1.ppc.rpm 
  fi
  if test -e gd-progs-1.8.4-3.aix5.1.ppc.rpm ; then
    rm gd-progs-1.8.4-3.aix5.1.ppc.rpm 
  fi
  if test -e gd-1.8.4-3.aix5.1.ppc.rpm ; then
    rm gd-1.8.4-3.aix5.1.ppc.rpm 
  fi
  if test -e gimp-libgimp-1.2.2-1.aix4.3.ppc.rpm ; then
    rm gimp-libgimp-1.2.2-1.aix4.3.ppc.rpm 
  fi
  if test -e gimp-1.2.2-1.aix4.3.ppc.rpm ; then
    rm gimp-1.2.2-1.aix4.3.ppc.rpm 
  fi
  if test -e ImageMagick-5.4.2-3.aix5.1.ppc.rpm ; then
    rm ImageMagick-5.4.2-3.aix5.1.ppc.rpm 
  fi
  if test -e libungif-progs-4.1.2-1.aix5.1.ppc.rpm ; then
    rm libungif-progs-4.1.2-1.aix5.1.ppc.rpm 
  fi
  if test -e transfig-3.2.3d-2.aix4.3.ppc.rpm ; then
    rm transfig-3.2.3d-2.aix4.3.ppc.rpm 
  fi
  if test -e xfig-3.2.3d-2.aix4.3.ppc.rpm ; then
    rm xfig-3.2.3d-2.aix4.3.ppc.rpm 
  fi
  if test -e xpaint-2.6.1-3.aix4.3.ppc.rpm ; then
    rm xpaint-2.6.1-3.aix4.3.ppc.rpm 
  fi
  if test -e xrender-0.8.4-7.aix5.1.ppc.rpm ; then
    rm xrender-0.8.4-7.aix5.1.ppc.rpm 
  fi
}
function do_GetEterm_clean { 
  if test -e Eterm-backgrounds-0.8.10-5.aix4.3.ppc.rpm ; then
    rm Eterm-backgrounds-0.8.10-5.aix4.3.ppc.rpm 
  fi
  if test -e Eterm-0.8.10-5.aix4.3.ppc.rpm ; then
    rm Eterm-0.8.10-5.aix4.3.ppc.rpm 
  fi
}
function do_GetScript_clean { 
  if test -e expect-5.42.1-3.aix5.1.ppc.rpm ; then
    rm expect-5.42.1-3.aix5.1.ppc.rpm 
  fi
  if test -e perl-5.8.2-1.aix5.1.ppc.rpm ; then
    rm perl-5.8.2-1.aix5.1.ppc.rpm 
  fi
  if test -e python-docs-2.3.4-2.aix5.1.ppc.rpm ; then
    rm python-docs-2.3.4-2.aix5.1.ppc.rpm 
  fi
  if test -e python-tools-2.3.4-2.aix5.1.ppc.rpm ; then
    rm python-tools-2.3.4-2.aix5.1.ppc.rpm 
  fi
  if test -e python-2.3.4-2.aix5.1.ppc.rpm ; then
    rm python-2.3.4-2.aix5.1.ppc.rpm 
  fi
  if test -e slang-1.4.4-2.aix4.3.ppc.rpm ; then
    rm slang-1.4.4-2.aix4.3.ppc.rpm 
  fi
  if test -e tcl-8.4.7-3.aix5.1.ppc.rpm ; then
    rm tcl-8.4.7-3.aix5.1.ppc.rpm 
  fi
  if test -e tk-8.4.7-3.aix5.1.ppc.rpm ; then
    rm tk-8.4.7-3.aix5.1.ppc.rpm 
  fi
  if test -e tkinter-2.3.4-3.aix5.1.ppc.rpm ; then
    rm tkinter-2.3.4-3.aix5.1.ppc.rpm 
  fi
}
function do_GetGuile_clean { 
  if test -e guile-1.4-3.aix4.3.ppc.rpm ; then
    rm guile-1.4-3.aix4.3.ppc.rpm 
  fi
}
function do_GetVnc_clean { 
  if test -e vnc-3.3.3r2-6.aix5.1.ppc.rpm ; then
    rm vnc-3.3.3r2-6.aix5.1.ppc.rpm 
  fi
}
function do_GetFtp_clean { 
  if test -e ftpcopy-0.3.9-1.aix4.3.ppc.rpm ; then
    rm ftpcopy-0.3.9-1.aix4.3.ppc.rpm 
  fi
  if test -e ncftp-3.1.1-3.aix4.3.ppc.rpm ; then
    rm ncftp-3.1.1-3.aix4.3.ppc.rpm 
  fi
  if test -e proftpd-1.2.8-1.aix5.1.ppc.rpm ; then
    rm proftpd-1.2.8-1.aix5.1.ppc.rpm 
  fi
  if test -e wu-ftpd-2.6.2-6.aix5.1.ppc.rpm ; then
    rm wu-ftpd-2.6.2-6.aix5.1.ppc.rpm 
  fi
}
function do_GetFileMgmt_clean { 
  if test -e rdist-6.1.5-2.aix4.3.ppc.rpm ; then
    rm rdist-6.1.5-2.aix4.3.ppc.rpm 
  fi
  if test -e rsync-2.6.2-1.aix5.1.ppc.rpm ; then
    rm rsync-2.6.2-1.aix5.1.ppc.rpm 
  fi
  if test -e zoo-2.10-5.aix5.1.ppc.rpm ; then
    rm zoo-2.10-5.aix5.1.ppc.rpm 
  fi
}
function do_GetAtk_clean { 
  if test -e atk-1.10.3-2.aix5.1.ppc.rpm ; then
    rm atk-1.10.3-2.aix5.1.ppc.rpm 
  fi
}
function do_GetPostScript_clean { 
  if test -e a2ps-4.13-2.aix4.3.ppc.rpm ; then
    rm a2ps-4.13-2.aix4.3.ppc.rpm 
  fi
  if test -e enscript-1.6.1-3.aix4.3.ppc.rpm ; then
    rm enscript-1.6.1-3.aix4.3.ppc.rpm 
  fi
  if test -e mpage-2.5-2.aix4.3.ppc.rpm ; then
    rm mpage-2.5-2.aix4.3.ppc.rpm 
  fi
  if test -e trueprint-5.3-1.aix4.3.ppc.rpm ; then
    rm trueprint-5.3-1.aix4.3.ppc.rpm 
  fi
  if test -e urw-fonts-2.0-1.aix4.3.noarch.rpm ; then
    rm urw-fonts-2.0-1.aix4.3.noarch.rpm 
  fi
}
function do_GetGhostScript_clean { 
  if test -e ghostscript-fonts-6.0-1.aix4.3.noarch.rpm ; then
    rm ghostscript-fonts-6.0-1.aix4.3.noarch.rpm 
  fi
  if test -e ghostscript-5.50-4.aix4.3.ppc.rpm ; then
    rm ghostscript-5.50-4.aix4.3.ppc.rpm 
  fi
  if test -e gv-3.5.8-4.aix4.3.ppc.rpm ; then
    rm gv-3.5.8-4.aix4.3.ppc.rpm 
  fi
}
function do_GetPdf_clean { 
  if test -e xpdf-1.00-1.aix4.3.ppc.rpm ; then
    rm xpdf-1.00-1.aix4.3.ppc.rpm 
  fi
}
function do_GetMicrosoft_clean { 
  if test -e mtools-3.9.8-3.aix4.3.ppc.rpm ; then
    rm mtools-3.9.8-3.aix4.3.ppc.rpm 
  fi
  if test -e samba-client-2.2.7-4.aix4.3.ppc.rpm ; then
    rm samba-client-2.2.7-4.aix4.3.ppc.rpm 
  fi
  if test -e samba-common-2.2.7-4.aix4.3.ppc.rpm ; then
    rm samba-common-2.2.7-4.aix4.3.ppc.rpm 
  fi
  if test -e samba-2.2.7-4.aix4.3.ppc.rpm ; then
    rm samba-2.2.7-4.aix4.3.ppc.rpm 
  fi
}
function do_GetMySql_clean { 
  if test -e MySQL-client-3.23.58-2.aix5.1.ppc.rpm ; then
    rm MySQL-client-3.23.58-2.aix5.1.ppc.rpm 
  fi
  if test -e MySQL-3.23.58-2.aix5.1.ppc.rpm ; then
    rm MySQL-3.23.58-2.aix5.1.ppc.rpm 
  fi
}
function do_GetDb_clean { 
  if test -e db-3.3.11-4.aix5.1.ppc.rpm ; then
    rm db-3.3.11-4.aix5.1.ppc.rpm 
  fi
}
function do_GetGdbm_clean { 
  if test -e gdbm-1.8.3-2.aix5.1.ppc.rpm ; then
    rm gdbm-1.8.3-2.aix5.1.ppc.rpm 
  fi
}
function do_GetMedia_clean { 
  if test -e audiofile-0.2.5-1.aix5.1.ppc.rpm ; then
    rm audiofile-0.2.5-1.aix5.1.ppc.rpm 
  fi
  if test -e esound-0.2.34-1.aix5.1.ppc.rpm ; then
    rm esound-0.2.34-1.aix5.1.ppc.rpm 
  fi
  if test -e xmms-1.2.7-1.aix4.3.ppc.rpm ; then
    rm xmms-1.2.7-1.aix4.3.ppc.rpm 
  fi
}
function do_GetCd_clean { 
  if test -e cdda2wav-1.9-4.aix4.3.ppc.rpm ; then
    rm cdda2wav-1.9-4.aix4.3.ppc.rpm 
  fi
  if test -e cdrecord-1.9-4.aix4.3.ppc.rpm ; then
    rm cdrecord-1.9-4.aix4.3.ppc.rpm 
  fi
  if test -e cdrecord-1.9-7.aix5.2.ppc.rpm ; then
    rm cdrecord-1.9-7.aix5.2.ppc.rpm 
  fi
  if test -e mkisofs-1.13-4.aix4.3.ppc.rpm ; then
    rm mkisofs-1.13-4.aix4.3.ppc.rpm 
  fi
  if test -e xmcd-3.0.2-1.aix4.3.ppc.rpm ; then
    rm xmcd-3.0.2-1.aix4.3.ppc.rpm 
  fi
}
function do_GetSpell_clean { 
  if test -e aspell-0.33.6.3-5.aix4.3.ppc.rpm ; then
    rm aspell-0.33.6.3-5.aix4.3.ppc.rpm 
  fi
  if test -e pspell-0.12.2-2.aix4.3.ppc.rpm ; then
    rm pspell-0.12.2-2.aix4.3.ppc.rpm 
  fi
}
function do_DevSpell_clean { 
  if test -e aspell-devel-0.33.6.3-4.aix4.3.ppc.rpm ; then
    rm aspell-devel-0.33.6.3-4.aix4.3.ppc.rpm 
  fi
  if test -e pspell-devel-0.12.2-2.aix4.3.ppc.rpm ; then
    rm pspell-devel-0.12.2-2.aix4.3.ppc.rpm 
  fi
}
function do_GetCal_clean { 
  if test -e gcal-3.01-1.aix4.3.ppc.rpm ; then
    rm gcal-3.01-1.aix4.3.ppc.rpm 
  fi
}
function do_GetGraph_clean { 
  if test -e gnuplot-3.7.1-1.aix4.3.ppc.rpm ; then
    rm gnuplot-3.7.1-1.aix4.3.ppc.rpm 
  fi
  if test -e plotutils-2.4.1-1.aix4.3.ppc.rpm ; then
    rm plotutils-2.4.1-1.aix4.3.ppc.rpm 
  fi
}
function do_GetGroff_clean { 
  if test -e groff-gxditview-1.17.2-1.aix4.3.ppc.rpm ; then
    rm groff-gxditview-1.17.2-1.aix4.3.ppc.rpm 
  fi
  if test -e groff-1.17.2-1.aix4.3.ppc.rpm ; then
    rm groff-1.17.2-1.aix4.3.ppc.rpm 
  fi
}
function do_GetPhp_clean { 
  if test -e php-manual-4.0.6-5.aix4.3.ppc.rpm ; then
    rm php-manual-4.0.6-5.aix4.3.ppc.rpm 
  fi
  if test -e php-4.0.6-5.aix4.3.ppc.rpm ; then
    rm php-4.0.6-5.aix4.3.ppc.rpm 
  fi
}
function do_GetApache_clean { 
  if test -e apache-manual-1.3.31-1.aix5.1.ppc.rpm ; then
    rm apache-manual-1.3.31-1.aix5.1.ppc.rpm 
  fi
  if test -e apache-1.3.31-1.aix5.1.ppc.rpm ; then
    rm apache-1.3.31-1.aix5.1.ppc.rpm 
  fi
}
function do_GetChat_clean { 
  if test -e gaim-0.52-2.aix4.3.ppc.rpm ; then
    rm gaim-0.52-2.aix4.3.ppc.rpm 
  fi
  if test -e jabber-1.4.2-1.aix4.3.ppc.rpm ; then
    rm jabber-1.4.2-1.aix4.3.ppc.rpm 
  fi
  if test -e ytalk-3.1.1-2.aix4.3.ppc.rpm ; then
    rm ytalk-3.1.1-2.aix4.3.ppc.rpm 
  fi
}
function do_GetMail_clean { 
  if test -e elm-2.5.6-2.aix4.3.ppc.rpm ; then
    rm elm-2.5.6-2.aix4.3.ppc.rpm 
  fi
  if test -e fetchmail-5.9.10-1.aix4.3.ppc.rpm ; then
    rm fetchmail-5.9.10-1.aix4.3.ppc.rpm 
  fi
  if test -e fetchmailconf-5.9.10-1.aix4.3.ppc.rpm ; then
    rm fetchmailconf-5.9.10-1.aix4.3.ppc.rpm 
  fi
  if test -e metamail-2.7-2.aix4.3.ppc.rpm ; then
    rm metamail-2.7-2.aix4.3.ppc.rpm 
  fi
  if test -e mutt-1.4.2.1-1.aix5.1.ppc.rpm ; then
    rm mutt-1.4.2.1-1.aix5.1.ppc.rpm 
  fi
  if test -e pine-4.44-3.aix4.3.ppc.rpm ; then
    rm pine-4.44-3.aix4.3.ppc.rpm 
  fi
  if test -e procmail-3.21-1.aix4.3.ppc.rpm ; then
    rm procmail-3.21-1.aix4.3.ppc.rpm 
  fi
}
function do_GetFont_clean { 
  if test -e fnlib-0.5-4.aix4.3.ppc.rpm ; then
    rm fnlib-0.5-4.aix4.3.ppc.rpm 
  fi
  if test -e fontconfig-2.2.2-6.aix5.2.ppc.rpm ; then
    rm fontconfig-2.2.2-6.aix5.2.ppc.rpm 
  fi
  if test -e freetype-demo-1.3.1-9.aix5.1.ppc.rpm ; then
    rm freetype-demo-1.3.1-9.aix5.1.ppc.rpm 
  fi
  if test -e freetype-1.3.1-9.aix5.1.ppc.rpm ; then
    rm freetype-1.3.1-9.aix5.1.ppc.rpm 
  fi
  if test -e xft-2.1.6-5.aix5.1.ppc.rpm ; then
    rm xft-2.1.6-5.aix5.1.ppc.rpm 
  fi
}
function do_DevFont_clean { 
  if test -e fnlib-devel-0.5-4.aix4.3.ppc.rpm ; then
    rm fnlib-devel-0.5-4.aix4.3.ppc.rpm 
  fi
  if test -e fontconfig-devel-2.2.2-6.aix5.2.ppc.rpm ; then
    rm fontconfig-devel-2.2.2-6.aix5.2.ppc.rpm 
  fi
  if test -e freetype-devel-1.3.1-9.aix5.1.ppc.rpm ; then
    rm freetype-devel-1.3.1-9.aix5.1.ppc.rpm 
  fi
}
function do_GetFont2_clean { 
  if test -e freetype2-2.1.7-5.aix5.1.ppc.rpm ; then
    rm freetype2-2.1.7-5.aix5.1.ppc.rpm 
  fi
}
function do_DevFont2_clean { 
  if test -e freetype2-devel-2.1.7-5.aix5.1.ppc.rpm ; then
    rm freetype2-devel-2.1.7-5.aix5.1.ppc.rpm 
  fi
}
function do_GetXbase2_clean { 
  if test -e glib2-2.8.1-3.aix5.1.ppc.rpm ; then
    rm glib2-2.8.1-3.aix5.1.ppc.rpm 
  fi
  if test -e gtk2-engines-2.2.0-2.aix5.1.ppc.rpm ; then
    rm gtk2-engines-2.2.0-2.aix5.1.ppc.rpm 
  fi
  if test -e gtk2-2.8.3-9.aix5.1.ppc.rpm ; then
    rm gtk2-2.8.3-9.aix5.1.ppc.rpm 
  fi
}
function do_DevXbase2_clean { 
  if test -e glib2-devel-2.8.1-3.aix5.1.ppc.rpm ; then
    rm glib2-devel-2.8.1-3.aix5.1.ppc.rpm 
  fi
  if test -e gtk2-devel-2.8.3-9.aix5.1.ppc.rpm ; then
    rm gtk2-devel-2.8.3-9.aix5.1.ppc.rpm 
  fi
}
function do_GetCimom_clean { 
  if test -e openCIMOM-0.7-4.aix5.1.noarch.rpm ; then
    rm openCIMOM-0.7-4.aix5.1.noarch.rpm 
  fi
  if test -e openCIMOM-0.8-1.aix5.2.noarch.rpm ; then
    rm openCIMOM-0.8-1.aix5.2.noarch.rpm 
  fi
  if test -e pegasus-1.0-9.aix5.1.ppc.rpm ; then
    rm pegasus-1.0-9.aix5.1.ppc.rpm 
  fi
}
function do_GetModem_clean { 
  if test -e lrzsz-0.12.20-2.aix4.3.ppc.rpm ; then
    rm lrzsz-0.12.20-2.aix4.3.ppc.rpm 
  fi
}
function do_GetCompileTools_clean { 
  if test -e autoconf213-2.13-1.aix5.1.noarch.rpm ; then
    rm autoconf213-2.13-1.aix5.1.noarch.rpm 
  fi
  if test -e autoconf-2.59-1.aix5.1.noarch.rpm ; then
    rm autoconf-2.59-1.aix5.1.noarch.rpm 
  fi
  if test -e automake-1.8.5-1.aix5.1.noarch.rpm ; then
    rm automake-1.8.5-1.aix5.1.noarch.rpm 
  fi
  if test -e binutils-2.14-3.aix5.1.ppc.rpm ; then
    rm binutils-2.14-3.aix5.1.ppc.rpm 
  fi
  if test -e bison-1.875-3.aix5.1.ppc.rpm ; then
    rm bison-1.875-3.aix5.1.ppc.rpm 
  fi
  if test -e coreutils-5.2.1-2.aix5.1.ppc.rpm ; then
    rm coreutils-5.2.1-2.aix5.1.ppc.rpm 
  fi
  if test -e cpio-2.5-1.aix5.1.ppc.rpm ; then
    rm cpio-2.5-1.aix5.1.ppc.rpm 
  fi
  if test -e ddd-3.3.1-2.aix4.3.ppc.rpm ; then
    rm ddd-3.3.1-2.aix4.3.ppc.rpm 
  fi
  if test -e diffutils-2.8.1-1.aix4.3.ppc.rpm ; then
    rm diffutils-2.8.1-1.aix4.3.ppc.rpm 
  fi
  if test -e ElectricFence-2.2.2-1.aix4.3.ppc.rpm ; then
    rm ElectricFence-2.2.2-1.aix4.3.ppc.rpm 
  fi
  if test -e flex-2.5.4a-6.aix4.3.ppc.rpm ; then
    rm flex-2.5.4a-6.aix4.3.ppc.rpm 
  fi
  if test -e gawk-3.1.3-1.aix5.1.ppc.rpm ; then
    rm gawk-3.1.3-1.aix5.1.ppc.rpm 
  fi
  if test -e gcc-cplusplus-4.0.0-1.aix5.1.ppc.rpm ; then
    rm gcc-cplusplus-4.0.0-1.aix5.1.ppc.rpm 
  fi
  if test -e gcc-cplusplus-4.0.0-1.aix5.2.ppc.rpm ; then
    rm gcc-cplusplus-4.0.0-1.aix5.2.ppc.rpm 
  fi
  if test -e gcc-cplusplus-4.0.0-1.aix5.3.ppc.rpm ; then
    rm gcc-cplusplus-4.0.0-1.aix5.3.ppc.rpm 
  fi
  if test -e gcc-4.0.0-1.aix5.1.ppc.rpm ; then
    rm gcc-4.0.0-1.aix5.1.ppc.rpm 
  fi
  if test -e gcc-4.0.0-1.aix5.2.ppc.rpm ; then
    rm gcc-4.0.0-1.aix5.2.ppc.rpm 
  fi
  if test -e gcc-4.0.0-1.aix5.3.ppc.rpm ; then
    rm gcc-4.0.0-1.aix5.3.ppc.rpm 
  fi
  if test -e gdb-6.0-1.aix5.1.ppc.rpm ; then
    rm gdb-6.0-1.aix5.1.ppc.rpm 
  fi
  if test -e help2man-1.29-1.aix4.3.noarch.rpm ; then
    rm help2man-1.29-1.aix4.3.noarch.rpm 
  fi
  if test -e indent-2.2.7-2.aix4.3.ppc.rpm ; then
    rm indent-2.2.7-2.aix4.3.ppc.rpm 
  fi
  if test -e intltool-0.27.2-1.aix5.1.ppc.rpm ; then
    rm intltool-0.27.2-1.aix5.1.ppc.rpm 
  fi
  if test -e libgcc-4.0.0-1.aix5.1.ppc.rpm ; then
    rm libgcc-4.0.0-1.aix5.1.ppc.rpm 
  fi
  if test -e libgcc-4.0.0-1.aix5.2.ppc.rpm ; then
    rm libgcc-4.0.0-1.aix5.2.ppc.rpm 
  fi
  if test -e libgcc-4.0.0-1.aix5.3.ppc.rpm ; then
    rm libgcc-4.0.0-1.aix5.3.ppc.rpm 
  fi
  if test -e libstdcplusplus-4.0.0-1.aix5.1.ppc.rpm ; then
    rm libstdcplusplus-4.0.0-1.aix5.1.ppc.rpm 
  fi
  if test -e libstdcplusplus-4.0.0-1.aix5.2.ppc.rpm ; then
    rm libstdcplusplus-4.0.0-1.aix5.2.ppc.rpm 
  fi
  if test -e libstdcplusplus-4.0.0-1.aix5.3.ppc.rpm ; then
    rm libstdcplusplus-4.0.0-1.aix5.3.ppc.rpm 
  fi
  if test -e libtool-1.5.8-1.aix5.1.ppc.rpm ; then
    rm libtool-1.5.8-1.aix5.1.ppc.rpm 
  fi
  if test -e m4-1.4.1-1.aix5.1.ppc.rpm ; then
    rm m4-1.4.1-1.aix5.1.ppc.rpm 
  fi
  if test -e make-3.80-1.aix5.1.ppc.rpm ; then
    rm make-3.80-1.aix5.1.ppc.rpm 
  fi
  if test -e pkg-config-0.19-2.aix5.1.ppc.rpm ; then
    rm pkg-config-0.19-2.aix5.1.ppc.rpm 
  fi
  if test -e pth-1.4.0-2.aix4.3.ppc.rpm ; then
    rm pth-1.4.0-2.aix4.3.ppc.rpm 
  fi
  if test -e qt-designer-3.0.3-1.aix4.3.ppc.rpm ; then
    rm qt-designer-3.0.3-1.aix4.3.ppc.rpm 
  fi
  if test -e qt-Xt-3.0.3-1.aix4.3.ppc.rpm ; then
    rm qt-Xt-3.0.3-1.aix4.3.ppc.rpm 
  fi
  if test -e qt-3.0.3-1.aix4.3.ppc.rpm ; then
    rm qt-3.0.3-1.aix4.3.ppc.rpm 
  fi
  if test -e smake-1.3.2-1.aix4.3.noarch.rpm ; then
    rm smake-1.3.2-1.aix4.3.noarch.rpm 
  fi
  if test -e splint-3.0.1.6-2.aix5.1.ppc.rpm ; then
    rm splint-3.0.1.6-2.aix5.1.ppc.rpm 
  fi
}
function do_GetCvs_clean { 
  if test -e cvs-1.11.17-3.aix5.1.ppc.rpm ; then
    rm cvs-1.11.17-3.aix5.1.ppc.rpm 
  fi
}
function do_GetRcs_clean { 
  if test -e rcs-5.7-2.aix5.1.ppc.rpm ; then
    rm rcs-5.7-2.aix5.1.ppc.rpm 
  fi
}
function do_GetTcpTools_clean { 
  if test -e ethereal-0.8.18-1.aix4.3.ppc.rpm ; then
    rm ethereal-0.8.18-1.aix4.3.ppc.rpm 
  fi
  if test -e libpcap-0.8.3-1.aix5.1.ppc.rpm ; then
    rm libpcap-0.8.3-1.aix5.1.ppc.rpm 
  fi
  if test -e tcpdump-3.8.1-1.aix5.1.ppc.rpm ; then
    rm tcpdump-3.8.1-1.aix5.1.ppc.rpm 
  fi
  if test -e traceroute-1.4a12-2.aix4.3.ppc.rpm ; then
    rm traceroute-1.4a12-2.aix4.3.ppc.rpm 
  fi
}
function do_GetDocbook_clean { 
  if test -e docbookx-4.1.2-2.aix5.1.noarch.rpm ; then
    rm docbookx-4.1.2-2.aix5.1.noarch.rpm 
  fi
}
function do_GetDejagnu_clean { 
  if test -e dejagnu-1.4.2-1.aix4.3.ppc.rpm ; then
    rm dejagnu-1.4.2-1.aix4.3.ppc.rpm 
  fi
}
function do_DevCompileTools_clean { 
  if test -e libstdcplusplus-devel-4.0.0-1.aix5.1.ppc.rpm ; then
    rm libstdcplusplus-devel-4.0.0-1.aix5.1.ppc.rpm 
  fi
  if test -e libstdcplusplus-devel-4.0.0-1.aix5.2.ppc.rpm ; then
    rm libstdcplusplus-devel-4.0.0-1.aix5.2.ppc.rpm 
  fi
  if test -e libstdcplusplus-devel-4.0.0-1.aix5.3.ppc.rpm ; then
    rm libstdcplusplus-devel-4.0.0-1.aix5.3.ppc.rpm 
  fi
  if test -e pth-devel-1.4.0-2.aix4.3.ppc.rpm ; then
    rm pth-devel-1.4.0-2.aix4.3.ppc.rpm 
  fi
  if test -e qt-devel-3.0.3-1.aix4.3.ppc.rpm ; then
    rm qt-devel-3.0.3-1.aix4.3.ppc.rpm 
  fi
  if test -e readline-devel-4.3-2.aix5.1.ppc.rpm ; then
    rm readline-devel-4.3-2.aix5.1.ppc.rpm 
  fi
  if test -e rpm-devel-3.0.5-41.aix5.1.ppc.rpm ; then
    rm rpm-devel-3.0.5-41.aix5.1.ppc.rpm 
  fi
}
function do_DevXbase_clean { 
  if test -e aalib-devel-1.2-1.aix4.3.ppc.rpm ; then
    rm aalib-devel-1.2-1.aix4.3.ppc.rpm 
  fi
  if test -e glib-devel-1.2.10-2.aix4.3.ppc.rpm ; then
    rm glib-devel-1.2.10-2.aix4.3.ppc.rpm 
  fi
  if test -e gtkplus-devel-1.2.10-4.aix5.1.ppc.rpm ; then
    rm gtkplus-devel-1.2.10-4.aix5.1.ppc.rpm 
  fi
  if test -e gtkhtml2-devel-2.4.0-2.aix5.1.ppc.rpm ; then
    rm gtkhtml2-devel-2.4.0-2.aix5.1.ppc.rpm 
  fi
  if test -e imlib-devel-1.9.11-1.aix4.3.ppc.rpm ; then
    rm imlib-devel-1.9.11-1.aix4.3.ppc.rpm 
  fi
  if test -e libjpeg-devel-6b-6.aix5.1.ppc.rpm ; then
    rm libjpeg-devel-6b-6.aix5.1.ppc.rpm 
  fi
  if test -e libmng-devel-1.0.3-2.aix4.3.ppc.rpm ; then
    rm libmng-devel-1.0.3-2.aix4.3.ppc.rpm 
  fi
  if test -e libpng-devel-1.2.8-6.aix5.2.ppc.rpm ; then
    rm libpng-devel-1.2.8-6.aix5.2.ppc.rpm 
  fi
  if test -e libtiff-devel-3.6.1-4.aix5.1.ppc.rpm ; then
    rm libtiff-devel-3.6.1-4.aix5.1.ppc.rpm 
  fi
  if test -e libungif-devel-4.1.2-1.aix5.1.ppc.rpm ; then
    rm libungif-devel-4.1.2-1.aix5.1.ppc.rpm 
  fi
  if test -e Xaw3d-devel-1.5-4.aix5.1.ppc.rpm ; then
    rm Xaw3d-devel-1.5-4.aix5.1.ppc.rpm 
  fi
  if test -e Xbae-devel-4.9.1-2.aix4.3.ppc.rpm ; then
    rm Xbae-devel-4.9.1-2.aix4.3.ppc.rpm 
  fi
  if test -e xcursor-devel-1.0.2-3.aix5.1.ppc.rpm ; then
    rm xcursor-devel-1.0.2-3.aix5.1.ppc.rpm 
  fi
  if test -e xpm-devel-3.4k-7.aix5.1.ppc.rpm ; then
    rm xpm-devel-3.4k-7.aix5.1.ppc.rpm 
  fi
}
function do_DevXimage_clean { 
  if test -e cairo-devel-1.0.2-6.aix5.1.ppc.rpm ; then
    rm cairo-devel-1.0.2-6.aix5.1.ppc.rpm 
  fi
  if test -e gd-devel-1.8.4-3.aix5.1.ppc.rpm ; then
    rm gd-devel-1.8.4-3.aix5.1.ppc.rpm 
  fi
  if test -e gimp-devel-1.2.2-1.aix4.3.ppc.rpm ; then
    rm gimp-devel-1.2.2-1.aix4.3.ppc.rpm 
  fi
  if test -e ImageMagick-devel-5.4.2-3.aix5.1.ppc.rpm ; then
    rm ImageMagick-devel-5.4.2-3.aix5.1.ppc.rpm 
  fi
}
function do_DevWbase_clean { 
  if test -e curl-devel-7.9.3-2.aix4.3.ppc.rpm ; then
    rm curl-devel-7.9.3-2.aix4.3.ppc.rpm 
  fi
  if test -e expat-devel-1.95.7-4.aix5.1.ppc.rpm ; then
    rm expat-devel-1.95.7-4.aix5.1.ppc.rpm 
  fi
  if test -e librep-devel-0.14-1.aix4.3.ppc.rpm ; then
    rm librep-devel-0.14-1.aix4.3.ppc.rpm 
  fi
  if test -e libxml2-devel-2.6.21-2.aix5.1.ppc.rpm ; then
    rm libxml2-devel-2.6.21-2.aix5.1.ppc.rpm 
  fi
  if test -e libxslt-devel-1.1.5-2.aix5.1.ppc.rpm ; then
    rm libxslt-devel-1.1.5-2.aix5.1.ppc.rpm 
  fi
  if test -e pcre-devel-3.7-3.aix5.1.ppc.rpm ; then
    rm pcre-devel-3.7-3.aix5.1.ppc.rpm 
  fi
  if test -e zlib-devel-1.2.3-3.aix5.1.ppc.rpm ; then
    rm zlib-devel-1.2.3-3.aix5.1.ppc.rpm 
  fi
}
function do_DevScript_clean { 
  if test -e python-devel-2.3.4-2.aix5.1.ppc.rpm ; then
    rm python-devel-2.3.4-2.aix5.1.ppc.rpm 
  fi
  if test -e slang-devel-1.4.4-2.aix4.3.ppc.rpm ; then
    rm slang-devel-1.4.4-2.aix4.3.ppc.rpm 
  fi
}
function do_DevMedia_clean { 
  if test -e audiofile-devel-0.2.5-1.aix5.1.ppc.rpm ; then
    rm audiofile-devel-0.2.5-1.aix5.1.ppc.rpm 
  fi
  if test -e esound-devel-0.2.34-1.aix5.1.ppc.rpm ; then
    rm esound-devel-0.2.34-1.aix5.1.ppc.rpm 
  fi
  if test -e xmms-devel-1.2.7-1.aix4.3.ppc.rpm ; then
    rm xmms-devel-1.2.7-1.aix4.3.ppc.rpm 
  fi
}
function do_DevGdbm_clean { 
  if test -e gdbm-devel-1.8.3-2.aix5.1.ppc.rpm ; then
    rm gdbm-devel-1.8.3-2.aix5.1.ppc.rpm 
  fi
}
function do_DevGuile_clean { 
  if test -e guile-devel-1.4-3.aix4.3.ppc.rpm ; then
    rm guile-devel-1.4-3.aix4.3.ppc.rpm 
  fi
}
function do_DevCurses_clean { 
  if test -e ncurses-devel-5.2-3.aix4.3.ppc.rpm ; then
    rm ncurses-devel-5.2-3.aix4.3.ppc.rpm 
  fi
}
function do_DevPhp_clean { 
  if test -e php-devel-4.0.6-5.aix4.3.ppc.rpm ; then
    rm php-devel-4.0.6-5.aix4.3.ppc.rpm 
  fi
}
function do_DevApache_clean { 
  if test -e apache-devel-1.3.31-1.aix5.1.ppc.rpm ; then
    rm apache-devel-1.3.31-1.aix5.1.ppc.rpm 
  fi
}
function do_DevAtk_clean { 
  if test -e atk-devel-1.10.3-2.aix5.1.ppc.rpm ; then
    rm atk-devel-1.10.3-2.aix5.1.ppc.rpm 
  fi
}
function do_DevCd_clean { 
  if test -e cdrecord-devel-1.9-4.aix4.3.ppc.rpm ; then
    rm cdrecord-devel-1.9-4.aix4.3.ppc.rpm 
  fi
  if test -e cdrecord-devel-1.9-7.aix5.2.ppc.rpm ; then
    rm cdrecord-devel-1.9-7.aix5.2.ppc.rpm 
  fi
}
function do_DevMySql_clean { 
  if test -e MySQL-devel-3.23.58-2.aix5.1.ppc.rpm ; then
    rm MySQL-devel-3.23.58-2.aix5.1.ppc.rpm 
  fi
}
function do_GetGames_clean { 
  if test -e gnuchess-4.0.pl80-1.aix4.3.ppc.rpm ; then
    rm gnuchess-4.0.pl80-1.aix4.3.ppc.rpm 
  fi
  if test -e xboard-4.2.6-1.aix4.3.ppc.rpm ; then
    rm xboard-4.2.6-1.aix4.3.ppc.rpm 
  fi
}
function do_GetDeskTop_clean { 
  if test -e AfterStep-1.8.10-1.aix4.3.ppc.rpm ; then
    rm AfterStep-1.8.10-1.aix4.3.ppc.rpm 
  fi
  if test -e enlightenment-0.16.5-3.aix5.1.ppc.rpm ; then
    rm enlightenment-0.16.5-3.aix5.1.ppc.rpm 
  fi
  if test -e icewm-1.0.9-2.aix4.3.ppc.rpm ; then
    rm icewm-1.0.9-2.aix4.3.ppc.rpm 
  fi
  if test -e WindowMaker-0.65.1-2.aix4.3.ppc.rpm ; then
    rm WindowMaker-0.65.1-2.aix4.3.ppc.rpm 
  fi
  if test -e xscreensaver-4.06-2.aix5.1.ppc.rpm ; then
    rm xscreensaver-4.06-2.aix5.1.ppc.rpm 
  fi
}
# ################################################
# Uninstall/erase rpms from this machine (risky)
# ################################################
function do_GetBase_erase { 
  rpm --nodeps -e bash-doc 
  rpm --nodeps -e bash 
  rpm --nodeps -e bc 
  rpm --nodeps -e bzip2 
  rpm --nodeps -e findutils 
  rpm --nodeps -e gettext 
  rpm --nodeps -e git 
  rpm --nodeps -e grep 
  rpm --nodeps -e gzip 
  rpm --nodeps -e info 
  rpm --nodeps -e less 
  rpm --nodeps -e lsof 
  rpm --nodeps -e lsof 
  rpm --nodeps -e lsof 
  rpm --nodeps -e mawk 
  rpm --nodeps -e patch 
  rpm --nodeps -e popt 
  rpm --nodeps -e prngd 
  rpm --nodeps -e readline 
  rpm --nodeps -e rpm-build 
  rpm --nodeps -e rpm 
  rpm --nodeps -e rxvt 
  rpm --nodeps -e screen 
  rpm --nodeps -e sed 
  rpm --nodeps -e sharutils 
  rpm --nodeps -e sudo 
  rpm --nodeps -e tar 
  rpm --nodeps -e tcsh 
  rpm --nodeps -e unzip 
  rpm --nodeps -e which 
  rpm --nodeps -e zip 
  rpm --nodeps -e zsh 
}
function do_GetXbase_erase { 
  rpm --nodeps -e aalib 
  rpm --nodeps -e glib 
  rpm --nodeps -e gtk+ 
  rpm --nodeps -e gtk-doc 
  rpm --nodeps -e gtk-engines 
  rpm --nodeps -e gtkhtml2 
  rpm --nodeps -e libjpeg 
  rpm --nodeps -e libmng 
  rpm --nodeps -e libpng 
  rpm --nodeps -e libtiff 
  rpm --nodeps -e libungif 
  rpm --nodeps -e Xaw3d 
  rpm --nodeps -e Xbae 
  rpm --nodeps -e xcursor 
  rpm --nodeps -e xfce 
  rpm --nodeps -e xpm 
}
function do_GetWbase_erase { 
  rpm --nodeps -e curl 
  rpm --nodeps -e expat 
  rpm --nodeps -e jade 
  rpm --nodeps -e librep 
  rpm --nodeps -e libxml2 
  rpm --nodeps -e libxslt 
  rpm --nodeps -e lynx 
  rpm --nodeps -e pcre 
  rpm --nodeps -e squid 
  rpm --nodeps -e texinfo 
  rpm --nodeps -e webmin 
  rpm --nodeps -e wget 
  rpm --nodeps -e zlib 
}
function do_GetPropList_erase { 
  rpm --nodeps -e libPropList 
}
function do_GetCurses_erase { 
  rpm --nodeps -e ncurses 
}
function do_GetXedit_erase { 
  rpm --nodeps -e emacs-el 
  rpm --nodeps -e emacs-leim 
  rpm --nodeps -e emacs-nox 
  rpm --nodeps -e emacs-X11 
  rpm --nodeps -e emacs 
  rpm --nodeps -e hexedit 
  rpm --nodeps -e vim-common 
  rpm --nodeps -e vim-enhanced 
  rpm --nodeps -e vim-minimal 
  rpm --nodeps -e vim-X11 
  rpm --nodeps -e xemacs-el 
  rpm --nodeps -e xemacs-info 
  rpm --nodeps -e xemacs 
}
function do_GetXimage_erase { 
  rpm --nodeps -e cairo 
  rpm --nodeps -e gd-progs 
  rpm --nodeps -e gd 
  rpm --nodeps -e gimp-libgimp 
  rpm --nodeps -e gimp 
  rpm --nodeps -e ImageMagick 
  rpm --nodeps -e libungif-progs 
  rpm --nodeps -e transfig 
  rpm --nodeps -e xfig 
  rpm --nodeps -e xpaint 
  rpm --nodeps -e xrender 
}
function do_GetEterm_erase { 
  rpm --nodeps -e Eterm-backgrounds 
  rpm --nodeps -e Eterm 
}
function do_GetScript_erase { 
  rpm --nodeps -e expect 
  rpm --nodeps -e perl 
  rpm --nodeps -e python-docs 
  rpm --nodeps -e python-tools 
  rpm --nodeps -e python 
  rpm --nodeps -e slang 
  rpm --nodeps -e tcl 
  rpm --nodeps -e tk 
  rpm --nodeps -e tkinter 
}
function do_GetGuile_erase { 
  rpm --nodeps -e guile 
}
function do_GetVnc_erase { 
  rpm --nodeps -e vnc 
}
function do_GetFtp_erase { 
  rpm --nodeps -e ftpcopy 
  rpm --nodeps -e ncftp 
  rpm --nodeps -e proftpd 
  rpm --nodeps -e wu-ftpd 
}
function do_GetFileMgmt_erase { 
  rpm --nodeps -e rdist 
  rpm --nodeps -e rsync 
  rpm --nodeps -e zoo 
}
function do_GetAtk_erase { 
  rpm --nodeps -e atk 
}
function do_GetPostScript_erase { 
  rpm --nodeps -e a2ps 
  rpm --nodeps -e enscript 
  rpm --nodeps -e mpage 
  rpm --nodeps -e trueprint 
  rpm --nodeps -e urw-fonts 
}
function do_GetGhostScript_erase { 
  rpm --nodeps -e ghostscript-fonts 
  rpm --nodeps -e ghostscript 
  rpm --nodeps -e gv 
}
function do_GetPdf_erase { 
  rpm --nodeps -e xpdf 
}
function do_GetMicrosoft_erase { 
  rpm --nodeps -e mtools 
  rpm --nodeps -e samba-client 
  rpm --nodeps -e samba-common 
  rpm --nodeps -e samba 
}
function do_GetMySql_erase { 
  rpm --nodeps -e MySQL-client 
  rpm --nodeps -e MySQL 
}
function do_GetDb_erase { 
  rpm --nodeps -e db 
}
function do_GetGdbm_erase { 
  rpm --nodeps -e gdbm 
}
function do_GetMedia_erase { 
  rpm --nodeps -e audiofile 
  rpm --nodeps -e esound 
  rpm --nodeps -e xmms 
}
function do_GetCd_erase { 
  rpm --nodeps -e cdda2wav 
  rpm --nodeps -e cdrecord 
  rpm --nodeps -e cdrecord 
  rpm --nodeps -e mkisofs 
  rpm --nodeps -e xmcd 
}
function do_GetSpell_erase { 
  rpm --nodeps -e aspell 
  rpm --nodeps -e pspell 
}
function do_DevSpell_erase { 
  rpm --nodeps -e aspell-devel 
  rpm --nodeps -e pspell-devel 
}
function do_GetCal_erase { 
  rpm --nodeps -e gcal 
}
function do_GetGraph_erase { 
  rpm --nodeps -e gnuplot 
  rpm --nodeps -e plotutils 
}
function do_GetGroff_erase { 
  rpm --nodeps -e groff-gxditview 
  rpm --nodeps -e groff 
}
function do_GetPhp_erase { 
  rpm --nodeps -e php-manual 
  rpm --nodeps -e php 
}
function do_GetApache_erase { 
  rpm --nodeps -e apache-manual 
  rpm --nodeps -e apache 
}
function do_GetChat_erase { 
  rpm --nodeps -e gaim 
  rpm --nodeps -e jabber 
  rpm --nodeps -e ytalk 
}
function do_GetMail_erase { 
  rpm --nodeps -e elm 
  rpm --nodeps -e fetchmail 
  rpm --nodeps -e fetchmailconf 
  rpm --nodeps -e metamail 
  rpm --nodeps -e mutt 
  rpm --nodeps -e pine 
  rpm --nodeps -e procmail 
}
function do_GetFont_erase { 
  rpm --nodeps -e fnlib 
  rpm --nodeps -e fontconfig 
  rpm --nodeps -e freetype-demo 
  rpm --nodeps -e freetype 
  rpm --nodeps -e xft 
}
function do_DevFont_erase { 
  rpm --nodeps -e fnlib-devel 
  rpm --nodeps -e fontconfig-devel 
  rpm --nodeps -e freetype-devel 
}
function do_GetFont2_erase { 
  rpm --nodeps -e freetype2 
}
function do_DevFont2_erase { 
  rpm --nodeps -e freetype2-devel 
}
function do_GetXbase2_erase { 
  rpm --nodeps -e glib2 
  rpm --nodeps -e gtk2-engines 
  rpm --nodeps -e gtk2 
}
function do_DevXbase2_erase { 
  rpm --nodeps -e glib2-devel 
  rpm --nodeps -e gtk2-devel 
}
function do_GetCimom_erase { 
  rpm --nodeps -e openCIMOM 
  rpm --nodeps -e openCIMOM 
  rpm --nodeps -e pegasus 
}
function do_GetModem_erase { 
  rpm --nodeps -e lrzsz 
}
function do_GetCompileTools_erase { 
  rpm --nodeps -e autoconf213 
  rpm --nodeps -e autoconf 
  rpm --nodeps -e automake 
  rpm --nodeps -e binutils 
  rpm --nodeps -e bison 
  rpm --nodeps -e coreutils 
  rpm --nodeps -e cpio 
  rpm --nodeps -e ddd 
  rpm --nodeps -e diffutils 
  rpm --nodeps -e ElectricFence 
  rpm --nodeps -e flex 
  rpm --nodeps -e gawk 
  rpm --nodeps -e gcc-c++ 
  rpm --nodeps -e gcc-c++ 
  rpm --nodeps -e gcc-c++ 
  rpm --nodeps -e gcc 
  rpm --nodeps -e gcc 
  rpm --nodeps -e gcc 
  rpm --nodeps -e gdb 
  rpm --nodeps -e help2man 
  rpm --nodeps -e indent 
  rpm --nodeps -e intltool 
  rpm --nodeps -e libgcc 
  rpm --nodeps -e libgcc 
  rpm --nodeps -e libgcc 
  rpm --nodeps -e libstdc++ 
  rpm --nodeps -e libstdc++ 
  rpm --nodeps -e libstdc++ 
  rpm --nodeps -e libtool 
  rpm --nodeps -e m4 
  rpm --nodeps -e make 
  rpm --nodeps -e pkg-config 
  rpm --nodeps -e pth 
  rpm --nodeps -e qt-designer 
  rpm --nodeps -e qt-Xt 
  rpm --nodeps -e qt 
  rpm --nodeps -e smake 
  rpm --nodeps -e splint 
}
function do_GetCvs_erase { 
  rpm --nodeps -e cvs 
}
function do_GetRcs_erase { 
  rpm --nodeps -e rcs 
}
function do_GetTcpTools_erase { 
  rpm --nodeps -e ethereal 
  rpm --nodeps -e libpcap 
  rpm --nodeps -e tcpdump 
  rpm --nodeps -e traceroute 
}
function do_GetDocbook_erase { 
  rpm --nodeps -e docbookx 
}
function do_GetDejagnu_erase { 
  rpm --nodeps -e dejagnu 
}
function do_DevCompileTools_erase { 
  rpm --nodeps -e libstdc++-devel 
  rpm --nodeps -e libstdc++-devel 
  rpm --nodeps -e libstdc++-devel 
  rpm --nodeps -e pth-devel 
  rpm --nodeps -e qt-devel 
  rpm --nodeps -e readline-devel 
  rpm --nodeps -e rpm-devel 
}
function do_DevXbase_erase { 
  rpm --nodeps -e aalib-devel 
  rpm --nodeps -e glib-devel 
  rpm --nodeps -e gtk+-devel 
  rpm --nodeps -e gtkhtml2-devel 
  rpm --nodeps -e imlib-devel 
  rpm --nodeps -e libjpeg-devel 
  rpm --nodeps -e libmng-devel 
  rpm --nodeps -e libpng-devel 
  rpm --nodeps -e libtiff-devel 
  rpm --nodeps -e libungif-devel 
  rpm --nodeps -e Xaw3d-devel 
  rpm --nodeps -e Xbae-devel 
  rpm --nodeps -e xcursor-devel 
  rpm --nodeps -e xpm-devel 
}
function do_DevXimage_erase { 
  rpm --nodeps -e cairo-devel 
  rpm --nodeps -e gd-devel 
  rpm --nodeps -e gimp-devel 
  rpm --nodeps -e ImageMagick-devel 
}
function do_DevWbase_erase { 
  rpm --nodeps -e curl-devel 
  rpm --nodeps -e expat-devel 
  rpm --nodeps -e librep-devel 
  rpm --nodeps -e libxml2-devel 
  rpm --nodeps -e libxslt-devel 
  rpm --nodeps -e pcre-devel 
  rpm --nodeps -e zlib-devel 
}
function do_DevScript_erase { 
  rpm --nodeps -e python-devel 
  rpm --nodeps -e slang-devel 
}
function do_DevMedia_erase { 
  rpm --nodeps -e audiofile-devel 
  rpm --nodeps -e esound-devel 
  rpm --nodeps -e xmms-devel 
}
function do_DevGdbm_erase { 
  rpm --nodeps -e gdbm-devel 
}
function do_DevGuile_erase { 
  rpm --nodeps -e guile-devel 
}
function do_DevCurses_erase { 
  rpm --nodeps -e ncurses-devel 
}
function do_DevPhp_erase { 
  rpm --nodeps -e php-devel 
}
function do_DevApache_erase { 
  rpm --nodeps -e apache-devel 
}
function do_DevAtk_erase { 
  rpm --nodeps -e atk-devel 
}
function do_DevCd_erase { 
  rpm --nodeps -e cdrecord-devel 
  rpm --nodeps -e cdrecord-devel 
}
function do_DevMySql_erase { 
  rpm --nodeps -e MySQL-devel 
}
function do_GetGames_erase { 
  rpm --nodeps -e gnuchess 
  rpm --nodeps -e xboard 
}
function do_GetDeskTop_erase { 
  rpm --nodeps -e AfterStep 
  rpm --nodeps -e enlightenment 
  rpm --nodeps -e icewm 
  rpm --nodeps -e WindowMaker 
  rpm --nodeps -e xscreensaver 
}
# ################################################
# Main Menu
# ################################################
cd /QOpenSys/download
case "$1" in
  "")
  echo "wwwinstall.sh - 1.0.1"
  echo "Blank is not an accepted parameter, try --help."
  ;;
  "GetBase")
  do_GetBase_clean
  do_GetBase_wget
  do_GetBase_install
  ;;
  "GetBase_wget")
  do_GetBase_clean
  do_GetBase_wget
  ;;
  "GetBase_install")
  do_GetBase_install
  ;;
  "GetBase_clean")
  do_GetBase_clean
  ;;
  "GetBase_erase")
  do_GetBase_erase
  ;;
  "GetXbase")
  do_GetXbase_clean
  do_GetXbase_wget
  do_GetXbase_install
  ;;
  "GetXbase_wget")
  do_GetXbase_clean
  do_GetXbase_wget
  ;;
  "GetXbase_install")
  do_GetXbase_install
  ;;
  "GetXbase_clean")
  do_GetXbase_clean
  ;;
  "GetXbase_erase")
  do_GetXbase_erase
  ;;
  "GetWbase")
  do_GetWbase_clean
  do_GetWbase_wget
  do_GetWbase_install
  ;;
  "GetWbase_wget")
  do_GetWbase_clean
  do_GetWbase_wget
  ;;
  "GetWbase_install")
  do_GetWbase_install
  ;;
  "GetWbase_clean")
  do_GetWbase_clean
  ;;
  "GetWbase_erase")
  do_GetWbase_erase
  ;;
  "GetPropList")
  do_GetPropList_clean
  do_GetPropList_wget
  do_GetPropList_install
  ;;
  "GetPropList_wget")
  do_GetPropList_clean
  do_GetPropList_wget
  ;;
  "GetPropList_install")
  do_GetPropList_install
  ;;
  "GetPropList_clean")
  do_GetPropList_clean
  ;;
  "GetPropList_erase")
  do_GetPropList_erase
  ;;
  "GetCurses")
  do_GetCurses_clean
  do_GetCurses_wget
  do_GetCurses_install
  ;;
  "GetCurses_wget")
  do_GetCurses_clean
  do_GetCurses_wget
  ;;
  "GetCurses_install")
  do_GetCurses_install
  ;;
  "GetCurses_clean")
  do_GetCurses_clean
  ;;
  "GetCurses_erase")
  do_GetCurses_erase
  ;;
  "GetXedit")
  do_GetXedit_clean
  do_GetXedit_wget
  do_GetXedit_install
  ;;
  "GetXedit_wget")
  do_GetXedit_clean
  do_GetXedit_wget
  ;;
  "GetXedit_install")
  do_GetXedit_install
  ;;
  "GetXedit_clean")
  do_GetXedit_clean
  ;;
  "GetXedit_erase")
  do_GetXedit_erase
  ;;
  "GetXimage")
  do_GetXimage_clean
  do_GetXimage_wget
  do_GetXimage_install
  ;;
  "GetXimage_wget")
  do_GetXimage_clean
  do_GetXimage_wget
  ;;
  "GetXimage_install")
  do_GetXimage_install
  ;;
  "GetXimage_clean")
  do_GetXimage_clean
  ;;
  "GetXimage_erase")
  do_GetXimage_erase
  ;;
  "GetEterm")
  do_GetEterm_clean
  do_GetEterm_wget
  do_GetEterm_install
  ;;
  "GetEterm_wget")
  do_GetEterm_clean
  do_GetEterm_wget
  ;;
  "GetEterm_install")
  do_GetEterm_install
  ;;
  "GetEterm_clean")
  do_GetEterm_clean
  ;;
  "GetEterm_erase")
  do_GetEterm_erase
  ;;
  "GetScript")
  do_GetScript_clean
  do_GetScript_wget
  do_GetScript_install
  ;;
  "GetScript_wget")
  do_GetScript_clean
  do_GetScript_wget
  ;;
  "GetScript_install")
  do_GetScript_install
  ;;
  "GetScript_clean")
  do_GetScript_clean
  ;;
  "GetScript_erase")
  do_GetScript_erase
  ;;
  "GetGuile")
  do_GetGuile_clean
  do_GetGuile_wget
  do_GetGuile_install
  ;;
  "GetGuile_wget")
  do_GetGuile_clean
  do_GetGuile_wget
  ;;
  "GetGuile_install")
  do_GetGuile_install
  ;;
  "GetGuile_clean")
  do_GetGuile_clean
  ;;
  "GetGuile_erase")
  do_GetGuile_erase
  ;;
  "GetVnc")
  do_GetVnc_clean
  do_GetVnc_wget
  do_GetVnc_install
  ;;
  "GetVnc_wget")
  do_GetVnc_clean
  do_GetVnc_wget
  ;;
  "GetVnc_install")
  do_GetVnc_install
  ;;
  "GetVnc_clean")
  do_GetVnc_clean
  ;;
  "GetVnc_erase")
  do_GetVnc_erase
  ;;
  "GetFtp")
  do_GetFtp_clean
  do_GetFtp_wget
  do_GetFtp_install
  ;;
  "GetFtp_wget")
  do_GetFtp_clean
  do_GetFtp_wget
  ;;
  "GetFtp_install")
  do_GetFtp_install
  ;;
  "GetFtp_clean")
  do_GetFtp_clean
  ;;
  "GetFtp_erase")
  do_GetFtp_erase
  ;;
  "GetFileMgmt")
  do_GetFileMgmt_clean
  do_GetFileMgmt_wget
  do_GetFileMgmt_install
  ;;
  "GetFileMgmt_wget")
  do_GetFileMgmt_clean
  do_GetFileMgmt_wget
  ;;
  "GetFileMgmt_install")
  do_GetFileMgmt_install
  ;;
  "GetFileMgmt_clean")
  do_GetFileMgmt_clean
  ;;
  "GetFileMgmt_erase")
  do_GetFileMgmt_erase
  ;;
  "GetAtk")
  do_GetAtk_clean
  do_GetAtk_wget
  do_GetAtk_install
  ;;
  "GetAtk_wget")
  do_GetAtk_clean
  do_GetAtk_wget
  ;;
  "GetAtk_install")
  do_GetAtk_install
  ;;
  "GetAtk_clean")
  do_GetAtk_clean
  ;;
  "GetAtk_erase")
  do_GetAtk_erase
  ;;
  "GetPostScript")
  do_GetPostScript_clean
  do_GetPostScript_wget
  do_GetPostScript_install
  ;;
  "GetPostScript_wget")
  do_GetPostScript_clean
  do_GetPostScript_wget
  ;;
  "GetPostScript_install")
  do_GetPostScript_install
  ;;
  "GetPostScript_clean")
  do_GetPostScript_clean
  ;;
  "GetPostScript_erase")
  do_GetPostScript_erase
  ;;
  "GetGhostScript")
  do_GetGhostScript_clean
  do_GetGhostScript_wget
  do_GetGhostScript_install
  ;;
  "GetGhostScript_wget")
  do_GetGhostScript_clean
  do_GetGhostScript_wget
  ;;
  "GetGhostScript_install")
  do_GetGhostScript_install
  ;;
  "GetGhostScript_clean")
  do_GetGhostScript_clean
  ;;
  "GetGhostScript_erase")
  do_GetGhostScript_erase
  ;;
  "GetPdf")
  do_GetPdf_clean
  do_GetPdf_wget
  do_GetPdf_install
  ;;
  "GetPdf_wget")
  do_GetPdf_clean
  do_GetPdf_wget
  ;;
  "GetPdf_install")
  do_GetPdf_install
  ;;
  "GetPdf_clean")
  do_GetPdf_clean
  ;;
  "GetPdf_erase")
  do_GetPdf_erase
  ;;
  "GetMicrosoft")
  do_GetMicrosoft_clean
  do_GetMicrosoft_wget
  do_GetMicrosoft_install
  ;;
  "GetMicrosoft_wget")
  do_GetMicrosoft_clean
  do_GetMicrosoft_wget
  ;;
  "GetMicrosoft_install")
  do_GetMicrosoft_install
  ;;
  "GetMicrosoft_clean")
  do_GetMicrosoft_clean
  ;;
  "GetMicrosoft_erase")
  do_GetMicrosoft_erase
  ;;
  "GetMySql")
  do_GetMySql_clean
  do_GetMySql_wget
  do_GetMySql_install
  ;;
  "GetMySql_wget")
  do_GetMySql_clean
  do_GetMySql_wget
  ;;
  "GetMySql_install")
  do_GetMySql_install
  ;;
  "GetMySql_clean")
  do_GetMySql_clean
  ;;
  "GetMySql_erase")
  do_GetMySql_erase
  ;;
  "GetDb")
  do_GetDb_clean
  do_GetDb_wget
  do_GetDb_install
  ;;
  "GetDb_wget")
  do_GetDb_clean
  do_GetDb_wget
  ;;
  "GetDb_install")
  do_GetDb_install
  ;;
  "GetDb_clean")
  do_GetDb_clean
  ;;
  "GetDb_erase")
  do_GetDb_erase
  ;;
  "GetGdbm")
  do_GetGdbm_clean
  do_GetGdbm_wget
  do_GetGdbm_install
  ;;
  "GetGdbm_wget")
  do_GetGdbm_clean
  do_GetGdbm_wget
  ;;
  "GetGdbm_install")
  do_GetGdbm_install
  ;;
  "GetGdbm_clean")
  do_GetGdbm_clean
  ;;
  "GetGdbm_erase")
  do_GetGdbm_erase
  ;;
  "GetMedia")
  do_GetMedia_clean
  do_GetMedia_wget
  do_GetMedia_install
  ;;
  "GetMedia_wget")
  do_GetMedia_clean
  do_GetMedia_wget
  ;;
  "GetMedia_install")
  do_GetMedia_install
  ;;
  "GetMedia_clean")
  do_GetMedia_clean
  ;;
  "GetMedia_erase")
  do_GetMedia_erase
  ;;
  "GetCd")
  do_GetCd_clean
  do_GetCd_wget
  do_GetCd_install
  ;;
  "GetCd_wget")
  do_GetCd_clean
  do_GetCd_wget
  ;;
  "GetCd_install")
  do_GetCd_install
  ;;
  "GetCd_clean")
  do_GetCd_clean
  ;;
  "GetCd_erase")
  do_GetCd_erase
  ;;
  "GetSpell")
  do_GetSpell_clean
  do_GetSpell_wget
  do_GetSpell_install
  ;;
  "GetSpell_wget")
  do_GetSpell_clean
  do_GetSpell_wget
  ;;
  "GetSpell_install")
  do_GetSpell_install
  ;;
  "GetSpell_clean")
  do_GetSpell_clean
  ;;
  "GetSpell_erase")
  do_GetSpell_erase
  ;;
  "DevSpell")
  do_DevSpell_clean
  do_DevSpell_wget
  do_DevSpell_install
  ;;
  "DevSpell_wget")
  do_DevSpell_clean
  do_DevSpell_wget
  ;;
  "DevSpell_install")
  do_DevSpell_install
  ;;
  "DevSpell_clean")
  do_DevSpell_clean
  ;;
  "DevSpell_erase")
  do_DevSpell_erase
  ;;
  "GetCal")
  do_GetCal_clean
  do_GetCal_wget
  do_GetCal_install
  ;;
  "GetCal_wget")
  do_GetCal_clean
  do_GetCal_wget
  ;;
  "GetCal_install")
  do_GetCal_install
  ;;
  "GetCal_clean")
  do_GetCal_clean
  ;;
  "GetCal_erase")
  do_GetCal_erase
  ;;
  "GetGraph")
  do_GetGraph_clean
  do_GetGraph_wget
  do_GetGraph_install
  ;;
  "GetGraph_wget")
  do_GetGraph_clean
  do_GetGraph_wget
  ;;
  "GetGraph_install")
  do_GetGraph_install
  ;;
  "GetGraph_clean")
  do_GetGraph_clean
  ;;
  "GetGraph_erase")
  do_GetGraph_erase
  ;;
  "GetGroff")
  do_GetGroff_clean
  do_GetGroff_wget
  do_GetGroff_install
  ;;
  "GetGroff_wget")
  do_GetGroff_clean
  do_GetGroff_wget
  ;;
  "GetGroff_install")
  do_GetGroff_install
  ;;
  "GetGroff_clean")
  do_GetGroff_clean
  ;;
  "GetGroff_erase")
  do_GetGroff_erase
  ;;
  "GetPhp")
  do_GetPhp_clean
  do_GetPhp_wget
  do_GetPhp_install
  ;;
  "GetPhp_wget")
  do_GetPhp_clean
  do_GetPhp_wget
  ;;
  "GetPhp_install")
  do_GetPhp_install
  ;;
  "GetPhp_clean")
  do_GetPhp_clean
  ;;
  "GetPhp_erase")
  do_GetPhp_erase
  ;;
  "GetApache")
  do_GetApache_clean
  do_GetApache_wget
  do_GetApache_install
  ;;
  "GetApache_wget")
  do_GetApache_clean
  do_GetApache_wget
  ;;
  "GetApache_install")
  do_GetApache_install
  ;;
  "GetApache_clean")
  do_GetApache_clean
  ;;
  "GetApache_erase")
  do_GetApache_erase
  ;;
  "GetChat")
  do_GetChat_clean
  do_GetChat_wget
  do_GetChat_install
  ;;
  "GetChat_wget")
  do_GetChat_clean
  do_GetChat_wget
  ;;
  "GetChat_install")
  do_GetChat_install
  ;;
  "GetChat_clean")
  do_GetChat_clean
  ;;
  "GetChat_erase")
  do_GetChat_erase
  ;;
  "GetMail")
  do_GetMail_clean
  do_GetMail_wget
  do_GetMail_install
  ;;
  "GetMail_wget")
  do_GetMail_clean
  do_GetMail_wget
  ;;
  "GetMail_install")
  do_GetMail_install
  ;;
  "GetMail_clean")
  do_GetMail_clean
  ;;
  "GetMail_erase")
  do_GetMail_erase
  ;;
  "GetFont")
  do_GetFont_clean
  do_GetFont_wget
  do_GetFont_install
  ;;
  "GetFont_wget")
  do_GetFont_clean
  do_GetFont_wget
  ;;
  "GetFont_install")
  do_GetFont_install
  ;;
  "GetFont_clean")
  do_GetFont_clean
  ;;
  "GetFont_erase")
  do_GetFont_erase
  ;;
  "DevFont")
  do_DevFont_clean
  do_DevFont_wget
  do_DevFont_install
  ;;
  "DevFont_wget")
  do_DevFont_clean
  do_DevFont_wget
  ;;
  "DevFont_install")
  do_DevFont_install
  ;;
  "DevFont_clean")
  do_DevFont_clean
  ;;
  "DevFont_erase")
  do_DevFont_erase
  ;;
  "GetFont2")
  do_GetFont2_clean
  do_GetFont2_wget
  do_GetFont2_install
  ;;
  "GetFont2_wget")
  do_GetFont2_clean
  do_GetFont2_wget
  ;;
  "GetFont2_install")
  do_GetFont2_install
  ;;
  "GetFont2_clean")
  do_GetFont2_clean
  ;;
  "GetFont2_erase")
  do_GetFont2_erase
  ;;
  "DevFont2")
  do_DevFont2_clean
  do_DevFont2_wget
  do_DevFont2_install
  ;;
  "DevFont2_wget")
  do_DevFont2_clean
  do_DevFont2_wget
  ;;
  "DevFont2_install")
  do_DevFont2_install
  ;;
  "DevFont2_clean")
  do_DevFont2_clean
  ;;
  "DevFont2_erase")
  do_DevFont2_erase
  ;;
  "GetXbase2")
  do_GetXbase2_clean
  do_GetXbase2_wget
  do_GetXbase2_install
  ;;
  "GetXbase2_wget")
  do_GetXbase2_clean
  do_GetXbase2_wget
  ;;
  "GetXbase2_install")
  do_GetXbase2_install
  ;;
  "GetXbase2_clean")
  do_GetXbase2_clean
  ;;
  "GetXbase2_erase")
  do_GetXbase2_erase
  ;;
  "DevXbase2")
  do_DevXbase2_clean
  do_DevXbase2_wget
  do_DevXbase2_install
  ;;
  "DevXbase2_wget")
  do_DevXbase2_clean
  do_DevXbase2_wget
  ;;
  "DevXbase2_install")
  do_DevXbase2_install
  ;;
  "DevXbase2_clean")
  do_DevXbase2_clean
  ;;
  "DevXbase2_erase")
  do_DevXbase2_erase
  ;;
  "GetCimom")
  do_GetCimom_clean
  do_GetCimom_wget
  do_GetCimom_install
  ;;
  "GetCimom_wget")
  do_GetCimom_clean
  do_GetCimom_wget
  ;;
  "GetCimom_install")
  do_GetCimom_install
  ;;
  "GetCimom_clean")
  do_GetCimom_clean
  ;;
  "GetCimom_erase")
  do_GetCimom_erase
  ;;
  "GetModem")
  do_GetModem_clean
  do_GetModem_wget
  do_GetModem_install
  ;;
  "GetModem_wget")
  do_GetModem_clean
  do_GetModem_wget
  ;;
  "GetModem_install")
  do_GetModem_install
  ;;
  "GetModem_clean")
  do_GetModem_clean
  ;;
  "GetModem_erase")
  do_GetModem_erase
  ;;
  "GetCompileTools")
  do_GetCompileTools_clean
  do_GetCompileTools_wget
  do_GetCompileTools_install
  ;;
  "GetCompileTools_wget")
  do_GetCompileTools_clean
  do_GetCompileTools_wget
  ;;
  "GetCompileTools_install")
  do_GetCompileTools_install
  ;;
  "GetCompileTools_clean")
  do_GetCompileTools_clean
  ;;
  "GetCompileTools_erase")
  do_GetCompileTools_erase
  ;;
  "GetCvs")
  do_GetCvs_clean
  do_GetCvs_wget
  do_GetCvs_install
  ;;
  "GetCvs_wget")
  do_GetCvs_clean
  do_GetCvs_wget
  ;;
  "GetCvs_install")
  do_GetCvs_install
  ;;
  "GetCvs_clean")
  do_GetCvs_clean
  ;;
  "GetCvs_erase")
  do_GetCvs_erase
  ;;
  "GetRcs")
  do_GetRcs_clean
  do_GetRcs_wget
  do_GetRcs_install
  ;;
  "GetRcs_wget")
  do_GetRcs_clean
  do_GetRcs_wget
  ;;
  "GetRcs_install")
  do_GetRcs_install
  ;;
  "GetRcs_clean")
  do_GetRcs_clean
  ;;
  "GetRcs_erase")
  do_GetRcs_erase
  ;;
  "GetTcpTools")
  do_GetTcpTools_clean
  do_GetTcpTools_wget
  do_GetTcpTools_install
  ;;
  "GetTcpTools_wget")
  do_GetTcpTools_clean
  do_GetTcpTools_wget
  ;;
  "GetTcpTools_install")
  do_GetTcpTools_install
  ;;
  "GetTcpTools_clean")
  do_GetTcpTools_clean
  ;;
  "GetTcpTools_erase")
  do_GetTcpTools_erase
  ;;
  "GetDocbook")
  do_GetDocbook_clean
  do_GetDocbook_wget
  do_GetDocbook_install
  ;;
  "GetDocbook_wget")
  do_GetDocbook_clean
  do_GetDocbook_wget
  ;;
  "GetDocbook_install")
  do_GetDocbook_install
  ;;
  "GetDocbook_clean")
  do_GetDocbook_clean
  ;;
  "GetDocbook_erase")
  do_GetDocbook_erase
  ;;
  "GetDejagnu")
  do_GetDejagnu_clean
  do_GetDejagnu_wget
  do_GetDejagnu_install
  ;;
  "GetDejagnu_wget")
  do_GetDejagnu_clean
  do_GetDejagnu_wget
  ;;
  "GetDejagnu_install")
  do_GetDejagnu_install
  ;;
  "GetDejagnu_clean")
  do_GetDejagnu_clean
  ;;
  "GetDejagnu_erase")
  do_GetDejagnu_erase
  ;;
  "DevCompileTools")
  do_DevCompileTools_clean
  do_DevCompileTools_wget
  do_DevCompileTools_install
  ;;
  "DevCompileTools_wget")
  do_DevCompileTools_clean
  do_DevCompileTools_wget
  ;;
  "DevCompileTools_install")
  do_DevCompileTools_install
  ;;
  "DevCompileTools_clean")
  do_DevCompileTools_clean
  ;;
  "DevCompileTools_erase")
  do_DevCompileTools_erase
  ;;
  "DevXbase")
  do_DevXbase_clean
  do_DevXbase_wget
  do_DevXbase_install
  ;;
  "DevXbase_wget")
  do_DevXbase_clean
  do_DevXbase_wget
  ;;
  "DevXbase_install")
  do_DevXbase_install
  ;;
  "DevXbase_clean")
  do_DevXbase_clean
  ;;
  "DevXbase_erase")
  do_DevXbase_erase
  ;;
  "DevXimage")
  do_DevXimage_clean
  do_DevXimage_wget
  do_DevXimage_install
  ;;
  "DevXimage_wget")
  do_DevXimage_clean
  do_DevXimage_wget
  ;;
  "DevXimage_install")
  do_DevXimage_install
  ;;
  "DevXimage_clean")
  do_DevXimage_clean
  ;;
  "DevXimage_erase")
  do_DevXimage_erase
  ;;
  "DevWbase")
  do_DevWbase_clean
  do_DevWbase_wget
  do_DevWbase_install
  ;;
  "DevWbase_wget")
  do_DevWbase_clean
  do_DevWbase_wget
  ;;
  "DevWbase_install")
  do_DevWbase_install
  ;;
  "DevWbase_clean")
  do_DevWbase_clean
  ;;
  "DevWbase_erase")
  do_DevWbase_erase
  ;;
  "DevScript")
  do_DevScript_clean
  do_DevScript_wget
  do_DevScript_install
  ;;
  "DevScript_wget")
  do_DevScript_clean
  do_DevScript_wget
  ;;
  "DevScript_install")
  do_DevScript_install
  ;;
  "DevScript_clean")
  do_DevScript_clean
  ;;
  "DevScript_erase")
  do_DevScript_erase
  ;;
  "DevMedia")
  do_DevMedia_clean
  do_DevMedia_wget
  do_DevMedia_install
  ;;
  "DevMedia_wget")
  do_DevMedia_clean
  do_DevMedia_wget
  ;;
  "DevMedia_install")
  do_DevMedia_install
  ;;
  "DevMedia_clean")
  do_DevMedia_clean
  ;;
  "DevMedia_erase")
  do_DevMedia_erase
  ;;
  "DevGdbm")
  do_DevGdbm_clean
  do_DevGdbm_wget
  do_DevGdbm_install
  ;;
  "DevGdbm_wget")
  do_DevGdbm_clean
  do_DevGdbm_wget
  ;;
  "DevGdbm_install")
  do_DevGdbm_install
  ;;
  "DevGdbm_clean")
  do_DevGdbm_clean
  ;;
  "DevGdbm_erase")
  do_DevGdbm_erase
  ;;
  "DevGuile")
  do_DevGuile_clean
  do_DevGuile_wget
  do_DevGuile_install
  ;;
  "DevGuile_wget")
  do_DevGuile_clean
  do_DevGuile_wget
  ;;
  "DevGuile_install")
  do_DevGuile_install
  ;;
  "DevGuile_clean")
  do_DevGuile_clean
  ;;
  "DevGuile_erase")
  do_DevGuile_erase
  ;;
  "DevCurses")
  do_DevCurses_clean
  do_DevCurses_wget
  do_DevCurses_install
  ;;
  "DevCurses_wget")
  do_DevCurses_clean
  do_DevCurses_wget
  ;;
  "DevCurses_install")
  do_DevCurses_install
  ;;
  "DevCurses_clean")
  do_DevCurses_clean
  ;;
  "DevCurses_erase")
  do_DevCurses_erase
  ;;
  "DevPhp")
  do_DevPhp_clean
  do_DevPhp_wget
  do_DevPhp_install
  ;;
  "DevPhp_wget")
  do_DevPhp_clean
  do_DevPhp_wget
  ;;
  "DevPhp_install")
  do_DevPhp_install
  ;;
  "DevPhp_clean")
  do_DevPhp_clean
  ;;
  "DevPhp_erase")
  do_DevPhp_erase
  ;;
  "DevApache")
  do_DevApache_clean
  do_DevApache_wget
  do_DevApache_install
  ;;
  "DevApache_wget")
  do_DevApache_clean
  do_DevApache_wget
  ;;
  "DevApache_install")
  do_DevApache_install
  ;;
  "DevApache_clean")
  do_DevApache_clean
  ;;
  "DevApache_erase")
  do_DevApache_erase
  ;;
  "DevAtk")
  do_DevAtk_clean
  do_DevAtk_wget
  do_DevAtk_install
  ;;
  "DevAtk_wget")
  do_DevAtk_clean
  do_DevAtk_wget
  ;;
  "DevAtk_install")
  do_DevAtk_install
  ;;
  "DevAtk_clean")
  do_DevAtk_clean
  ;;
  "DevAtk_erase")
  do_DevAtk_erase
  ;;
  "DevCd")
  do_DevCd_clean
  do_DevCd_wget
  do_DevCd_install
  ;;
  "DevCd_wget")
  do_DevCd_clean
  do_DevCd_wget
  ;;
  "DevCd_install")
  do_DevCd_install
  ;;
  "DevCd_clean")
  do_DevCd_clean
  ;;
  "DevCd_erase")
  do_DevCd_erase
  ;;
  "DevMySql")
  do_DevMySql_clean
  do_DevMySql_wget
  do_DevMySql_install
  ;;
  "DevMySql_wget")
  do_DevMySql_clean
  do_DevMySql_wget
  ;;
  "DevMySql_install")
  do_DevMySql_install
  ;;
  "DevMySql_clean")
  do_DevMySql_clean
  ;;
  "DevMySql_erase")
  do_DevMySql_erase
  ;;
  "GetGames")
  do_GetGames_clean
  do_GetGames_wget
  do_GetGames_install
  ;;
  "GetGames_wget")
  do_GetGames_clean
  do_GetGames_wget
  ;;
  "GetGames_install")
  do_GetGames_install
  ;;
  "GetGames_clean")
  do_GetGames_clean
  ;;
  "GetGames_erase")
  do_GetGames_erase
  ;;
  "GetDeskTop")
  do_GetDeskTop_clean
  do_GetDeskTop_wget
  do_GetDeskTop_install
  ;;
  "GetDeskTop_wget")
  do_GetDeskTop_clean
  do_GetDeskTop_wget
  ;;
  "GetDeskTop_install")
  do_GetDeskTop_install
  ;;
  "GetDeskTop_clean")
  do_GetDeskTop_clean
  ;;
  "GetDeskTop_erase")
  do_GetDeskTop_erase
  ;;
  *)
  echo "wwwinstall.sh [option]"
  echo "option:"
  echo " --------------"
  echo " GetBase - wget and install rpms"
  echo "  GetBase_wget - wget rpms only"
  echo "  GetBase_install - install rpms only"
  echo "  GetBase_clean - remove rpm files not product (after install)"
  echo "  GetBase_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  bash-doc bash bc bzip2 findutils gettext git"
  echo "            grep gzip info less lsof lsof lsof mawk"
  echo "            patch popt prngd readline rpm-build"
  echo "            rpm rxvt screen sed sharutils sudo tar"
  echo "            tcsh unzip which zip zsh"
  echo " --------------"
  echo " GetXbase - wget and install rpms"
  echo "  GetXbase_wget - wget rpms only"
  echo "  GetXbase_install - install rpms only"
  echo "  GetXbase_clean - remove rpm files not product (after install)"
  echo "  GetXbase_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  aalib glib gtk+ gtk-doc gtk-engines gtkhtml2"
  echo "            libjpeg libmng libpng libtiff libungif"
  echo "            Xaw3d Xbae xcursor xfce xpm"
  echo " --------------"
  echo " GetWbase - wget and install rpms"
  echo "  GetWbase_wget - wget rpms only"
  echo "  GetWbase_install - install rpms only"
  echo "  GetWbase_clean - remove rpm files not product (after install)"
  echo "  GetWbase_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  curl expat jade librep libxml2 libxslt lynx"
  echo "            pcre squid texinfo webmin wget zlib"
  echo " --------------"
  echo " GetPropList - wget and install rpms"
  echo "  GetPropList_wget - wget rpms only"
  echo "  GetPropList_install - install rpms only"
  echo "  GetPropList_clean - remove rpm files not product (after install)"
  echo "  GetPropList_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  libPropList"
  echo " --------------"
  echo " GetCurses - wget and install rpms"
  echo "  GetCurses_wget - wget rpms only"
  echo "  GetCurses_install - install rpms only"
  echo "  GetCurses_clean - remove rpm files not product (after install)"
  echo "  GetCurses_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  ncurses"
  echo " --------------"
  echo " GetXedit - wget and install rpms"
  echo "  GetXedit_wget - wget rpms only"
  echo "  GetXedit_install - install rpms only"
  echo "  GetXedit_clean - remove rpm files not product (after install)"
  echo "  GetXedit_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  emacs-el emacs-leim emacs-nox emacs-X11 emacs"
  echo "            hexedit vim-common vim-enhanced vim-minimal"
  echo "            vim-X11 xemacs-el xemacs-info"
  echo "            xemacs"
  echo " --------------"
  echo " GetXimage - wget and install rpms"
  echo "  GetXimage_wget - wget rpms only"
  echo "  GetXimage_install - install rpms only"
  echo "  GetXimage_clean - remove rpm files not product (after install)"
  echo "  GetXimage_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  cairo gd-progs gd gimp-libgimp gimp ImageMagick"
  echo "            libungif-progs transfig xfig xpaint"
  echo "            xrender"
  echo " --------------"
  echo " GetEterm - wget and install rpms"
  echo "  GetEterm_wget - wget rpms only"
  echo "  GetEterm_install - install rpms only"
  echo "  GetEterm_clean - remove rpm files not product (after install)"
  echo "  GetEterm_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  Eterm-backgrounds Eterm"
  echo " --------------"
  echo " GetScript - wget and install rpms"
  echo "  GetScript_wget - wget rpms only"
  echo "  GetScript_install - install rpms only"
  echo "  GetScript_clean - remove rpm files not product (after install)"
  echo "  GetScript_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  expect perl python-docs python-tools python"
  echo "            slang tcl tk tkinter"
  echo " --------------"
  echo " GetGuile - wget and install rpms"
  echo "  GetGuile_wget - wget rpms only"
  echo "  GetGuile_install - install rpms only"
  echo "  GetGuile_clean - remove rpm files not product (after install)"
  echo "  GetGuile_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  guile"
  echo " --------------"
  echo " GetVnc - wget and install rpms"
  echo "  GetVnc_wget - wget rpms only"
  echo "  GetVnc_install - install rpms only"
  echo "  GetVnc_clean - remove rpm files not product (after install)"
  echo "  GetVnc_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  vnc"
  echo " --------------"
  echo " GetFtp - wget and install rpms"
  echo "  GetFtp_wget - wget rpms only"
  echo "  GetFtp_install - install rpms only"
  echo "  GetFtp_clean - remove rpm files not product (after install)"
  echo "  GetFtp_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  ftpcopy ncftp proftpd wu-ftpd"
  echo " --------------"
  echo " GetFileMgmt - wget and install rpms"
  echo "  GetFileMgmt_wget - wget rpms only"
  echo "  GetFileMgmt_install - install rpms only"
  echo "  GetFileMgmt_clean - remove rpm files not product (after install)"
  echo "  GetFileMgmt_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  rdist rsync zoo"
  echo " --------------"
  echo " GetAtk - wget and install rpms"
  echo "  GetAtk_wget - wget rpms only"
  echo "  GetAtk_install - install rpms only"
  echo "  GetAtk_clean - remove rpm files not product (after install)"
  echo "  GetAtk_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  atk"
  echo " --------------"
  echo " GetPostScript - wget and install rpms"
  echo "  GetPostScript_wget - wget rpms only"
  echo "  GetPostScript_install - install rpms only"
  echo "  GetPostScript_clean - remove rpm files not product (after install)"
  echo "  GetPostScript_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  a2ps enscript mpage trueprint urw-fonts"
  echo " --------------"
  echo " GetGhostScript - wget and install rpms"
  echo "  GetGhostScript_wget - wget rpms only"
  echo "  GetGhostScript_install - install rpms only"
  echo "  GetGhostScript_clean - remove rpm files not product (after install)"
  echo "  GetGhostScript_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  ghostscript-fonts ghostscript gv"
  echo " --------------"
  echo " GetPdf - wget and install rpms"
  echo "  GetPdf_wget - wget rpms only"
  echo "  GetPdf_install - install rpms only"
  echo "  GetPdf_clean - remove rpm files not product (after install)"
  echo "  GetPdf_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  xpdf"
  echo " --------------"
  echo " GetMicrosoft - wget and install rpms"
  echo "  GetMicrosoft_wget - wget rpms only"
  echo "  GetMicrosoft_install - install rpms only"
  echo "  GetMicrosoft_clean - remove rpm files not product (after install)"
  echo "  GetMicrosoft_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  mtools samba-client samba-common samba"
  echo " --------------"
  echo " GetMySql - wget and install rpms"
  echo "  GetMySql_wget - wget rpms only"
  echo "  GetMySql_install - install rpms only"
  echo "  GetMySql_clean - remove rpm files not product (after install)"
  echo "  GetMySql_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  MySQL-client MySQL"
  echo " --------------"
  echo " GetDb - wget and install rpms"
  echo "  GetDb_wget - wget rpms only"
  echo "  GetDb_install - install rpms only"
  echo "  GetDb_clean - remove rpm files not product (after install)"
  echo "  GetDb_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  db"
  echo " --------------"
  echo " GetGdbm - wget and install rpms"
  echo "  GetGdbm_wget - wget rpms only"
  echo "  GetGdbm_install - install rpms only"
  echo "  GetGdbm_clean - remove rpm files not product (after install)"
  echo "  GetGdbm_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  gdbm"
  echo " --------------"
  echo " GetMedia - wget and install rpms"
  echo "  GetMedia_wget - wget rpms only"
  echo "  GetMedia_install - install rpms only"
  echo "  GetMedia_clean - remove rpm files not product (after install)"
  echo "  GetMedia_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  audiofile esound xmms"
  echo " --------------"
  echo " GetCd - wget and install rpms"
  echo "  GetCd_wget - wget rpms only"
  echo "  GetCd_install - install rpms only"
  echo "  GetCd_clean - remove rpm files not product (after install)"
  echo "  GetCd_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  cdda2wav cdrecord cdrecord mkisofs xmcd"
  echo " --------------"
  echo " GetSpell - wget and install rpms"
  echo "  GetSpell_wget - wget rpms only"
  echo "  GetSpell_install - install rpms only"
  echo "  GetSpell_clean - remove rpm files not product (after install)"
  echo "  GetSpell_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  aspell pspell"
  echo " --------------"
  echo " DevSpell - wget and install rpms"
  echo "  DevSpell_wget - wget rpms only"
  echo "  DevSpell_install - install rpms only"
  echo "  DevSpell_clean - remove rpm files not product (after install)"
  echo "  DevSpell_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  aspell-devel pspell-devel"
  echo " --------------"
  echo " GetCal - wget and install rpms"
  echo "  GetCal_wget - wget rpms only"
  echo "  GetCal_install - install rpms only"
  echo "  GetCal_clean - remove rpm files not product (after install)"
  echo "  GetCal_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  gcal"
  echo " --------------"
  echo " GetGraph - wget and install rpms"
  echo "  GetGraph_wget - wget rpms only"
  echo "  GetGraph_install - install rpms only"
  echo "  GetGraph_clean - remove rpm files not product (after install)"
  echo "  GetGraph_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  gnuplot plotutils"
  echo " --------------"
  echo " GetGroff - wget and install rpms"
  echo "  GetGroff_wget - wget rpms only"
  echo "  GetGroff_install - install rpms only"
  echo "  GetGroff_clean - remove rpm files not product (after install)"
  echo "  GetGroff_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  groff-gxditview groff"
  echo " --------------"
  echo " GetPhp - wget and install rpms"
  echo "  GetPhp_wget - wget rpms only"
  echo "  GetPhp_install - install rpms only"
  echo "  GetPhp_clean - remove rpm files not product (after install)"
  echo "  GetPhp_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  php-manual php"
  echo " --------------"
  echo " GetApache - wget and install rpms"
  echo "  GetApache_wget - wget rpms only"
  echo "  GetApache_install - install rpms only"
  echo "  GetApache_clean - remove rpm files not product (after install)"
  echo "  GetApache_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  apache-manual apache"
  echo " --------------"
  echo " GetChat - wget and install rpms"
  echo "  GetChat_wget - wget rpms only"
  echo "  GetChat_install - install rpms only"
  echo "  GetChat_clean - remove rpm files not product (after install)"
  echo "  GetChat_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  gaim jabber ytalk"
  echo " --------------"
  echo " GetMail - wget and install rpms"
  echo "  GetMail_wget - wget rpms only"
  echo "  GetMail_install - install rpms only"
  echo "  GetMail_clean - remove rpm files not product (after install)"
  echo "  GetMail_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  elm fetchmail fetchmailconf metamail mutt"
  echo "            pine procmail"
  echo " --------------"
  echo " GetFont - wget and install rpms"
  echo "  GetFont_wget - wget rpms only"
  echo "  GetFont_install - install rpms only"
  echo "  GetFont_clean - remove rpm files not product (after install)"
  echo "  GetFont_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  fnlib fontconfig freetype-demo freetype xft"
  echo " --------------"
  echo " DevFont - wget and install rpms"
  echo "  DevFont_wget - wget rpms only"
  echo "  DevFont_install - install rpms only"
  echo "  DevFont_clean - remove rpm files not product (after install)"
  echo "  DevFont_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  fnlib-devel fontconfig-devel freetype-devel"
  echo " --------------"
  echo " GetFont2 - wget and install rpms"
  echo "  GetFont2_wget - wget rpms only"
  echo "  GetFont2_install - install rpms only"
  echo "  GetFont2_clean - remove rpm files not product (after install)"
  echo "  GetFont2_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  freetype2"
  echo " --------------"
  echo " DevFont2 - wget and install rpms"
  echo "  DevFont2_wget - wget rpms only"
  echo "  DevFont2_install - install rpms only"
  echo "  DevFont2_clean - remove rpm files not product (after install)"
  echo "  DevFont2_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  freetype2-devel"
  echo " --------------"
  echo " GetXbase2 - wget and install rpms"
  echo "  GetXbase2_wget - wget rpms only"
  echo "  GetXbase2_install - install rpms only"
  echo "  GetXbase2_clean - remove rpm files not product (after install)"
  echo "  GetXbase2_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  glib2 gtk2-engines gtk2"
  echo " --------------"
  echo " DevXbase2 - wget and install rpms"
  echo "  DevXbase2_wget - wget rpms only"
  echo "  DevXbase2_install - install rpms only"
  echo "  DevXbase2_clean - remove rpm files not product (after install)"
  echo "  DevXbase2_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  glib2-devel gtk2-devel"
  echo " --------------"
  echo " GetCimom - wget and install rpms"
  echo "  GetCimom_wget - wget rpms only"
  echo "  GetCimom_install - install rpms only"
  echo "  GetCimom_clean - remove rpm files not product (after install)"
  echo "  GetCimom_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  openCIMOM openCIMOM pegasus"
  echo " --------------"
  echo " GetModem - wget and install rpms"
  echo "  GetModem_wget - wget rpms only"
  echo "  GetModem_install - install rpms only"
  echo "  GetModem_clean - remove rpm files not product (after install)"
  echo "  GetModem_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  lrzsz"
  echo " --------------"
  echo " GetCompileTools - wget and install rpms"
  echo "  GetCompileTools_wget - wget rpms only"
  echo "  GetCompileTools_install - install rpms only"
  echo "  GetCompileTools_clean - remove rpm files not product (after install)"
  echo "  GetCompileTools_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  autoconf213 autoconf automake binutils bison"
  echo "            coreutils cpio ddd diffutils ElectricFence"
  echo "            flex gawk gcc-c++ gcc-c++ gcc-c++"
  echo "            gcc gcc gcc gdb help2man indent intltool"
  echo "            libgcc libgcc libgcc libstdc++ libstdc++"
  echo "            libstdc++ libtool m4 make pkg-config"
  echo "            pth qt-designer qt-Xt qt smake splint"
  echo " --------------"
  echo " GetCvs - wget and install rpms"
  echo "  GetCvs_wget - wget rpms only"
  echo "  GetCvs_install - install rpms only"
  echo "  GetCvs_clean - remove rpm files not product (after install)"
  echo "  GetCvs_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  cvs"
  echo " --------------"
  echo " GetRcs - wget and install rpms"
  echo "  GetRcs_wget - wget rpms only"
  echo "  GetRcs_install - install rpms only"
  echo "  GetRcs_clean - remove rpm files not product (after install)"
  echo "  GetRcs_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  rcs"
  echo " --------------"
  echo " GetTcpTools - wget and install rpms"
  echo "  GetTcpTools_wget - wget rpms only"
  echo "  GetTcpTools_install - install rpms only"
  echo "  GetTcpTools_clean - remove rpm files not product (after install)"
  echo "  GetTcpTools_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  ethereal libpcap tcpdump traceroute"
  echo " --------------"
  echo " GetDocbook - wget and install rpms"
  echo "  GetDocbook_wget - wget rpms only"
  echo "  GetDocbook_install - install rpms only"
  echo "  GetDocbook_clean - remove rpm files not product (after install)"
  echo "  GetDocbook_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  docbookx"
  echo " --------------"
  echo " GetDejagnu - wget and install rpms"
  echo "  GetDejagnu_wget - wget rpms only"
  echo "  GetDejagnu_install - install rpms only"
  echo "  GetDejagnu_clean - remove rpm files not product (after install)"
  echo "  GetDejagnu_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  dejagnu"
  echo " --------------"
  echo " DevCompileTools - wget and install rpms"
  echo "  DevCompileTools_wget - wget rpms only"
  echo "  DevCompileTools_install - install rpms only"
  echo "  DevCompileTools_clean - remove rpm files not product (after install)"
  echo "  DevCompileTools_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  libstdc++-devel libstdc++-devel libstdc++-devel"
  echo "            pth-devel qt-devel readline-devel"
  echo "            rpm-devel"
  echo " --------------"
  echo " DevXbase - wget and install rpms"
  echo "  DevXbase_wget - wget rpms only"
  echo "  DevXbase_install - install rpms only"
  echo "  DevXbase_clean - remove rpm files not product (after install)"
  echo "  DevXbase_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  aalib-devel glib-devel gtk+-devel gtkhtml2-devel"
  echo "            imlib-devel libjpeg-devel libmng-devel"
  echo "            libpng-devel libtiff-devel libungif-devel"
  echo "            Xaw3d-devel Xbae-devel xcursor-devel"
  echo "            xpm-devel"
  echo " --------------"
  echo " DevXimage - wget and install rpms"
  echo "  DevXimage_wget - wget rpms only"
  echo "  DevXimage_install - install rpms only"
  echo "  DevXimage_clean - remove rpm files not product (after install)"
  echo "  DevXimage_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  cairo-devel gd-devel gimp-devel ImageMagick-devel"
  echo " --------------"
  echo " DevWbase - wget and install rpms"
  echo "  DevWbase_wget - wget rpms only"
  echo "  DevWbase_install - install rpms only"
  echo "  DevWbase_clean - remove rpm files not product (after install)"
  echo "  DevWbase_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  curl-devel expat-devel librep-devel libxml2-devel"
  echo "            libxslt-devel pcre-devel zlib-devel"
  echo " --------------"
  echo " DevScript - wget and install rpms"
  echo "  DevScript_wget - wget rpms only"
  echo "  DevScript_install - install rpms only"
  echo "  DevScript_clean - remove rpm files not product (after install)"
  echo "  DevScript_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  python-devel slang-devel"
  echo " --------------"
  echo " DevMedia - wget and install rpms"
  echo "  DevMedia_wget - wget rpms only"
  echo "  DevMedia_install - install rpms only"
  echo "  DevMedia_clean - remove rpm files not product (after install)"
  echo "  DevMedia_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  audiofile-devel esound-devel xmms-devel"
  echo " --------------"
  echo " DevGdbm - wget and install rpms"
  echo "  DevGdbm_wget - wget rpms only"
  echo "  DevGdbm_install - install rpms only"
  echo "  DevGdbm_clean - remove rpm files not product (after install)"
  echo "  DevGdbm_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  gdbm-devel"
  echo " --------------"
  echo " DevGuile - wget and install rpms"
  echo "  DevGuile_wget - wget rpms only"
  echo "  DevGuile_install - install rpms only"
  echo "  DevGuile_clean - remove rpm files not product (after install)"
  echo "  DevGuile_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  guile-devel"
  echo " --------------"
  echo " DevCurses - wget and install rpms"
  echo "  DevCurses_wget - wget rpms only"
  echo "  DevCurses_install - install rpms only"
  echo "  DevCurses_clean - remove rpm files not product (after install)"
  echo "  DevCurses_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  ncurses-devel"
  echo " --------------"
  echo " DevPhp - wget and install rpms"
  echo "  DevPhp_wget - wget rpms only"
  echo "  DevPhp_install - install rpms only"
  echo "  DevPhp_clean - remove rpm files not product (after install)"
  echo "  DevPhp_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  php-devel"
  echo " --------------"
  echo " DevApache - wget and install rpms"
  echo "  DevApache_wget - wget rpms only"
  echo "  DevApache_install - install rpms only"
  echo "  DevApache_clean - remove rpm files not product (after install)"
  echo "  DevApache_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  apache-devel"
  echo " --------------"
  echo " DevAtk - wget and install rpms"
  echo "  DevAtk_wget - wget rpms only"
  echo "  DevAtk_install - install rpms only"
  echo "  DevAtk_clean - remove rpm files not product (after install)"
  echo "  DevAtk_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  atk-devel"
  echo " --------------"
  echo " DevCd - wget and install rpms"
  echo "  DevCd_wget - wget rpms only"
  echo "  DevCd_install - install rpms only"
  echo "  DevCd_clean - remove rpm files not product (after install)"
  echo "  DevCd_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  cdrecord-devel cdrecord-devel"
  echo " --------------"
  echo " DevMySql - wget and install rpms"
  echo "  DevMySql_wget - wget rpms only"
  echo "  DevMySql_install - install rpms only"
  echo "  DevMySql_clean - remove rpm files not product (after install)"
  echo "  DevMySql_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  MySQL-devel"
  echo " --------------"
  echo " GetGames - wget and install rpms"
  echo "  GetGames_wget - wget rpms only"
  echo "  GetGames_install - install rpms only"
  echo "  GetGames_clean - remove rpm files not product (after install)"
  echo "  GetGames_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  gnuchess xboard"
  echo " --------------"
  echo " GetDeskTop - wget and install rpms"
  echo "  GetDeskTop_wget - wget rpms only"
  echo "  GetDeskTop_install - install rpms only"
  echo "  GetDeskTop_clean - remove rpm files not product (after install)"
  echo "  GetDeskTop_erase - uninstall/erase rpm packages (risky)"
  echo "   rpm(s):  AfterStep enlightenment icewm WindowMaker"
  echo "            xscreensaver"
  ;;
esac
